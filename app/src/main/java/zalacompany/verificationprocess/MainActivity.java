package zalacompany.verificationprocess;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

import zalacompany.verificationprocess.fragments.HomeFragment;
import zalacompany.verificationprocess.fragments.SelfieVerificationFragment;

public class MainActivity extends FragmentActivity {

    private Fragment currentFragment = null;
    public static WeakReference<MainActivity> wrActivity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        wrActivity = new WeakReference<MainActivity>(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getCurrentFragment() == null)
            setFragment(new HomeFragment(),false);
    }

    /**
     * Get the current fragment in the main activity container
     */
    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    /**
     * Set the current fragment in the main activity container
     */
    public void setCurrentFragment(Fragment f) {
        currentFragment = f;
    }

    /**
     * Fragment to be prepared, without arguments, animation is needed.
     *
     * @param f fragment instance to be shown in main activity.
     */
    public void setFragment(final Fragment f) {
        setFragment(f, null, true);
    }

    /**
     * Fragment to be prepared, without arguments.
     *
     * @param f    fragment instance to be shown in main activity.
     * @param anim define if an animation must be include
     */
    public void setFragment(final Fragment f, final boolean anim) {
        setFragment(f, null, anim);
    }

    /**
     * Fragment to be prepared.
     *
     * @param f   fragment instance to be shown in main activity.
     * @param i   arguments send to the fragment.
     * @param ani define if an animation must be include
     */
    public void setFragment(final Fragment f, final Bundle i, final boolean ani) {
        FragmentTransaction t;

        // argument check
        if (i != null)
            f.setArguments(i);

        // commit change
        FragmentManager fm = getSupportFragmentManager();
        t = fm.beginTransaction();
        // animation is needed
        if (ani)
            t.setCustomAnimations(R.anim.enter_right, // current out
                    R.anim.exit_left, // new in
                    R.anim.enter_revertright, // back in
                    R.anim.exit_revertleft // current out
            );

        t.replace(R.id.activity_main_mainContainer, f);
        t.addToBackStack(null);
        t.commit();
//        setFragmentHandler(f);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
