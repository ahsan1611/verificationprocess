package zalacompany.verificationprocess.webservice_pojomodel;

/**
 * Created by optimtec006 on 26/03/2017.
 */

public class VerificationAppResponse {
    private String BirthDateDescription;

    private String DOB;

    private String ExpirationDateStatus;

    private String FaceImg;

    private String UniqueID;

    private String DocumentExpired;

    private String DocumentStatus;

    private String IssueDateDescription;

    private String Address2;

    private String SurName;

    private String Address1;

    private String GivenName;

    private Boolean IsMatch;

    private String BirthDateStatus;

    private String IssueDateStatus;

    private String Sex;

    private String DocumentExpiredStatus;

    private String BirthDateVaild;

    private String IssueDateVaild;

    private String City;

    private String SignatureImg;

    private String DocumentClass;

    private String PostalCode;

    private String State;

    private String ExpirationDateVaild;

    private String ExpirationDateDescription;

    private String DocumentExpiredDescription;

    private Boolean IsError;

    private String Message;


    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }


    public String getBirthDateDescription ()
    {
        return BirthDateDescription;
    }

    public void setBirthDateDescription (String BirthDateDescription)
    {
        this.BirthDateDescription = BirthDateDescription;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public void setDOB (String DOB)
    {
        this.DOB = DOB;
    }

    public String getExpirationDateStatus ()
    {
        return ExpirationDateStatus;
    }

    public void setExpirationDateStatus (String ExpirationDateStatus)
    {
        this.ExpirationDateStatus = ExpirationDateStatus;
    }

    public String getFaceImg ()
    {
        return FaceImg;
    }

    public void setFaceImg (String FaceImg)
    {
        this.FaceImg = FaceImg;
    }

    public String getUniqueID ()
    {
        return UniqueID;
    }

    public void setUniqueID (String UniqueID)
    {
        this.UniqueID = UniqueID;
    }

    public String getDocumentExpired ()
    {
        return DocumentExpired;
    }

    public void setDocumentExpired (String DocumentExpired)
    {
        this.DocumentExpired = DocumentExpired;
    }

    public String getDocumentStatus ()
    {
        return DocumentStatus;
    }

    public void setDocumentStatus (String DocumentStatus)
    {
        this.DocumentStatus = DocumentStatus;
    }

    public String getIssueDateDescription ()
    {
        return IssueDateDescription;
    }

    public void setIssueDateDescription (String IssueDateDescription)
    {
        this.IssueDateDescription = IssueDateDescription;
    }

    public String getAddress2 ()
    {
        return Address2;
    }

    public void setAddress2 (String Address2)
    {
        this.Address2 = Address2;
    }

    public String getSurName ()
    {
        return SurName;
    }

    public void setSurName (String SurName)
    {
        this.SurName = SurName;
    }

    public String getAddress1 ()
    {
        return Address1;
    }

    public void setAddress1 (String Address1)
    {
        this.Address1 = Address1;
    }

    public String getGivenName ()
    {
        return GivenName;
    }

    public void setGivenName (String GivenName)
    {
        this.GivenName = GivenName;
    }

    public Boolean getIsMatch ()
    {
        return IsMatch;
    }

    public void setIsMatch (Boolean IsMatch)
    {
        this.IsMatch = IsMatch;
    }

    public String getBirthDateStatus ()
    {
        return BirthDateStatus;
    }

    public void setBirthDateStatus (String BirthDateStatus)
    {
        this.BirthDateStatus = BirthDateStatus;
    }

    public String getIssueDateStatus ()
    {
        return IssueDateStatus;
    }

    public void setIssueDateStatus (String IssueDateStatus)
    {
        this.IssueDateStatus = IssueDateStatus;
    }

    public String getSex ()
    {
        return Sex;
    }

    public void setSex (String Sex)
    {
        this.Sex = Sex;
    }

    public String getDocumentExpiredStatus ()
    {
        return DocumentExpiredStatus;
    }

    public void setDocumentExpiredStatus (String DocumentExpiredStatus)
    {
        this.DocumentExpiredStatus = DocumentExpiredStatus;
    }

    public String getBirthDateVaild ()
    {
        return BirthDateVaild;
    }

    public void setBirthDateVaild (String BirthDateVaild)
    {
        this.BirthDateVaild = BirthDateVaild;
    }

    public String getIssueDateVaild ()
    {
        return IssueDateVaild;
    }

    public void setIssueDateVaild (String IssueDateVaild)
    {
        this.IssueDateVaild = IssueDateVaild;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    public String getSignatureImg ()
    {
        return SignatureImg;
    }

    public void setSignatureImg (String SignatureImg)
    {
        this.SignatureImg = SignatureImg;
    }

    public String getDocumentClass ()
    {
        return DocumentClass;
    }

    public void setDocumentClass (String DocumentClass)
    {
        this.DocumentClass = DocumentClass;
    }

    public String getPostalCode ()
    {
        return PostalCode;
    }

    public void setPostalCode (String PostalCode)
    {
        this.PostalCode = PostalCode;
    }

    public String getState ()
    {
        return State;
    }

    public void setState (String State)
    {
        this.State = State;
    }

    public String getExpirationDateVaild ()
    {
        return ExpirationDateVaild;
    }

    public void setExpirationDateVaild (String ExpirationDateVaild)
    {
        this.ExpirationDateVaild = ExpirationDateVaild;
    }

    public String getExpirationDateDescription ()
    {
        return ExpirationDateDescription;
    }

    public void setExpirationDateDescription (String ExpirationDateDescription)
    {
        this.ExpirationDateDescription = ExpirationDateDescription;
    }

    public String getDocumentExpiredDescription ()
    {
        return DocumentExpiredDescription;
    }

    public void setDocumentExpiredDescription (String DocumentExpiredDescription)
    {
        this.DocumentExpiredDescription = DocumentExpiredDescription;
    }

    public Boolean getIsError ()
    {
        return IsError;
    }

    public void setIsError (Boolean IsError)
    {
        this.IsError = IsError;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [BirthDateDescription = "+BirthDateDescription+", DOB = "+DOB+", ExpirationDateStatus = "+ExpirationDateStatus+", FaceImg = "+FaceImg+", UniqueID = "+UniqueID+", DocumentExpired = "+DocumentExpired+", DocumentStatus = "+DocumentStatus+", IssueDateDescription = "+IssueDateDescription+", Address2 = "+Address2+", SurName = "+SurName+", Address1 = "+Address1+", GivenName = "+GivenName+", IsMatch = "+IsMatch+", BirthDateStatus = "+BirthDateStatus+", IssueDateStatus = "+IssueDateStatus+", Sex = "+Sex+", DocumentExpiredStatus = "+DocumentExpiredStatus+", BirthDateVaild = "+BirthDateVaild+", IssueDateVaild = "+IssueDateVaild+", City = "+City+", SignatureImg = "+SignatureImg+", DocumentClass = "+DocumentClass+", PostalCode = "+PostalCode+", State = "+State+", ExpirationDateVaild = "+ExpirationDateVaild+", ExpirationDateDescription = "+ExpirationDateDescription+", DocumentExpiredDescription = "+DocumentExpiredDescription+", IsError = "+IsError+"]";
    }
}
