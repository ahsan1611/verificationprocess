package zalacompany.verificationprocess.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

/**
 * Created by AhsanKamal on 18/09/2016.
 */
public class PrefSettings {

    protected final static String PREF_SETTINGS = "PREF_SETTINGS";
    protected final static String PREF_UNIQUEID = "uniqueid";


    private Context context;

    public PrefSettings(Context con) {
        context = con;
        // checkSettings();
    }

    private void setSetting(final String name, final String value) {
        SharedPreferences userDetails = context.getSharedPreferences(
                PREF_SETTINGS, FragmentActivity.MODE_PRIVATE);
        byte[] bs;
        String s;

        try {
            SharedPreferences.Editor edit = userDetails.edit();
            bs = value.getBytes("UTF-8");
            s = Base64.encodeToString(bs, Base64.DEFAULT);
            edit.putString(name, s);
            edit.commit();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public void setUniqueID(final String value) {
        SharedPreferences userDetails = context.getSharedPreferences(
                PREF_SETTINGS, FragmentActivity.MODE_PRIVATE);
        SharedPreferences.Editor edit = userDetails.edit();
        edit.putString(PREF_UNIQUEID, value);
        edit.commit();
    }

    //================= Getter ============================//
    public final String getUniqueID() {
        SharedPreferences userDetails = context.getSharedPreferences(
                PREF_SETTINGS, FragmentActivity.MODE_PRIVATE);
        return userDetails.getString(PREF_UNIQUEID, "");
    }

}
