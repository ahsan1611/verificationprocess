package zalacompany.verificationprocess.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

/**
 * Created by optimtec006 on 26/03/2017.
 */

public class UtilHelper {

    private static String charset = "UTF-8";

    public static String encodeImage(Bitmap bm)
    {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            String str = "data:image/false;base64,";
            byte[] strbyte = str.getBytes(charset);// 923452500267.h$5uhHz(8lm_&tgbBw//i58JhZ&$edF0&ht%re34$
            byte[] b = baos.toByteArray();
//            b = setDpi(b,300);
            String encImage = str+Base64.encodeToString(b, Base64.NO_WRAP);

            return encImage;
        }catch (UnsupportedEncodingException err){
            err.printStackTrace();
        }
        return "";
    }

    public static String encodeImage2(Bitmap bm)
    {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            String str = "data:image/jpg;base64,";
            byte[] strbyte = str.getBytes(charset);// 923452500267.h$5uhHz(8lm_&tgbBw//i58JhZ&$edF0&ht%re34$
            byte[] b = baos.toByteArray();
            b = setDpi(b,300);
            String encImage = Base64.encodeToString(b, Base64.NO_WRAP);

            return encImage;
        }catch (UnsupportedEncodingException err){
            err.printStackTrace();
        }
        return "";
    }

    private static byte[] setDpi(byte[] imageData, int dpi) {
        imageData[13] = 1;
        imageData[14] = (byte) (dpi >> 8);
        imageData[15] = (byte) (dpi & 0xff);
        imageData[16] = (byte) (dpi >> 8);
        imageData[17] = (byte) (dpi & 0xff);

        return imageData;
    }

    public static String encodeImage(byte[] bm)
    {
            String str = "data:image/false;base64,";
            bm = setDpi(bm,354);

        String encImage = str+Base64.encodeToString(bm, Base64.NO_WRAP);

            return encImage;
    }

    public static String encodeImage(String path)
    {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(imagefile);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;

    }

    public static Bitmap Base64toBitmap(String encodedImage){

        byte[] decodedString = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedByte;
    }

    public static String BitmapToByteArray(Bitmap bm){
        //calculate how many bytes our image consists of.
        int bytes = bm.getByteCount();

        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        bm.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

        byte[] array = buffer.array(); //Get the underlying array containing the data.
        String str = "data:image/false;base64,";
        String encImage = str+Base64.encodeToString(array, Base64.NO_WRAP);

        return encImage;
    }

    public static void appendLog(String text)
    {
        File logFile = new File("sdcard/log.txt");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
