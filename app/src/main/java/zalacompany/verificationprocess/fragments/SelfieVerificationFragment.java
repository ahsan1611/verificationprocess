package zalacompany.verificationprocess.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import zalacompany.verificationprocess.R;
import zalacompany.verificationprocess.utils.PrefSettings;
import zalacompany.verificationprocess.utils.UtilHelper;
import zalacompany.verificationprocess.webservice_pojomodel.VerificationAppResponse;

import static android.R.attr.width;
import static android.content.Context.WINDOW_SERVICE;
import static zalacompany.verificationprocess.R.attr.height;

/**
 * Created by optimtec006 on 27/03/2017.
 */

public class SelfieVerificationFragment extends BaseFragment {

    int cameraId = -1;
    private String encodingBase64Img = "";

    Camera camera;
    SurfaceHolder SH_cameraHolder;
    SurfaceView SV_cameraView;
    Button BT_verify;
    TextView TV_masg;


    Camera.PictureCallback rawCallback;
    Camera.ShutterCallback shutterCallback;
    Camera.PictureCallback jpegCallback;

    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selfieverification,container,false);

        bindUI(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        for(int i = 0; i< Camera.getNumberOfCameras(); i++){
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i,info);
            if(info.facing== Camera.CameraInfo.CAMERA_FACING_FRONT){
                cameraId = i;
                break;
            }
        }
        camera=Camera.open(cameraId);
        setCameraDisplayOrientation(getActivity(),cameraId,camera);
        camera.startPreview();
    }

    private void bindUI(View view){

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing your request...");
        progressDialog.setCanceledOnTouchOutside(false);

        TV_masg = (TextView)view.findViewById(R.id.fragment_selfieverification_TV_masg);
        SV_cameraView = (SurfaceView)view.findViewById(R.id.fragment_selfieverification_SV_Camera);
        SH_cameraHolder = SV_cameraView.getHolder();

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        SH_cameraHolder.addCallback(_surfaceHolderCallback);

        // deprecated setting, but required on Android versions prior to 3.0
        SH_cameraHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

//        android.widget.FrameLayout.LayoutParams params = new android.widget.FrameLayout.LayoutParams(width, height);
//        SV_cameraView.setLayoutParams(params);

        jpegCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                String timeStamp = new SimpleDateFormat( "yyyyMMdd_HHmmss").format( new Date( ));
                String output_file_name = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + timeStamp + ".jpg";

                File pictureFile = new File(output_file_name);
                if (pictureFile.exists()) {
                    pictureFile.delete();
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);

                    Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);

                    ExifInterface exif=new ExifInterface(pictureFile.toString());

                    Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                    if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")){
                        realImage= rotate(realImage, 90);
                    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")){
                        realImage= rotate(realImage, 270);
                    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")){
                        realImage= rotate(realImage, 180);
                    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")){
                        realImage= rotate(realImage, 270);
                    }

                    boolean bo = realImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);

//                    int w = realImage.getWidth();
//                    int h = realImage.getHeight();
//
//                    Log.v("RESPONSE","width: "+w+":: height: "+h);

                    fos.close();
                    encodingBase64Img = UtilHelper.encodeImage2(realImage);
                    UtilHelper.appendLog(encodingBase64Img);


                    PrefSettings pf = new PrefSettings(getActivity());
                    new VerificationImageAsync().execute(encodingBase64Img,pf.getUniqueID());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                }
//                Toast.makeText(getActivity(), "Picture Saved", Toast.LENGTH_LONG).show();
                refreshCamera();
            }
        };

        rawCallback = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

            }
        };

        shutterCallback = new Camera.ShutterCallback() {
            @Override
            public void onShutter() {

            }
        };

        BT_verify = (Button)view.findViewById(R.id.fragment_selfieverification_BT_verify);
        BT_verify.setOnClickListener(_click);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private View.OnClickListener _click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                captureImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    SurfaceHolder.Callback _surfaceHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                // open the camera
                camera = Camera.open();
            } catch (RuntimeException e) {
                // check for exceptions
                System.err.println(e);
                return;
            }
            setCameraDisplayOrientation(getActivity(),cameraId,camera);
            Camera.Parameters param;
//            param = camera.getParameters();
//            SV_cameraView.getLayoutParams().width=param.getPreviewSize().height;
//            SV_cameraView.getLayoutParams().height=param.getPreviewSize().width;

            // modify parameter
//            param.setPreviewSize(352, 288);
//            camera.setParameters(param);
            // make any resize, rotate or reformatting changes here
            try {
                // The Surface has been created, now tell the camera where to draw
                // the preview.
                camera.setPreviewDisplay(SH_cameraHolder);
                setCameraDisplayOrientation(getActivity(),cameraId,camera);

                camera.startPreview();
            } catch (Exception e) {
                // check for exceptions
                System.err.println(e);
                return;
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            // Now that the size is known, set up the camera parameters and begin
            // the preview.
            refreshCamera();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // stop preview and release camera
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    };

    public void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    public void captureImage() throws IOException {
        //take the picture
        camera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }

    public void refreshCamera() {
        if (SH_cameraHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera.setPreviewDisplay(SH_cameraHolder);
            setCameraDisplayOrientation(getActivity(),cameraId,camera);

            camera.startPreview();
        } catch (Exception e) {

        }
    }

    private class VerificationImageAsync extends AsyncTask<String,Void,String> {
        String xml;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://zala-kyc.com/api/VerificationApp/verify");
            httppost.addHeader("Authorization","OQ3tQXnKL2jYbWkkeu9yCg==");

            Log.e("RESPONSE",xmlString(params[0],params[1]));
            try {
                StringEntity se = new StringEntity(xmlString(params[0],params[1]), HTTP.UTF_8);
                se.setContentType("text/xml");

                httppost.setEntity(se);

                HttpResponse httpresponse = httpclient.execute(httppost);
                HttpEntity resEntity = httpresponse.getEntity();
                InputStream instream = resEntity.getContent();
                xml = readStream(instream);

                Log.v("RESPONSE",xml);
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }
            return xml;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();
            final Activity activity = getMainActivity().wrActivity.get();
            if (activity != null && !activity.isFinishing()) {
                if (xml != null) {
                    VerificationAppResponse model = gsonXml.fromXml(xml, VerificationAppResponse.class);
                    if (!model.getIsError()) {
                        TV_masg.setText(model.getMessage());

                    } else {
                        TV_masg.setText(model.getMessage());
                    }
                }
            }
        }
    }

    // Converting InputStream to String

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    XmlParserCreator parserCreator = new XmlParserCreator() {
        @Override
        public XmlPullParser createParser() {
            try {
                return XmlPullParserFactory.newInstance().newPullParser();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    };

    GsonXml gsonXml = new GsonXmlBuilder()
            .setXmlParserCreator(parserCreator)
            .create();

    private String xmlString(String imgdata,String uniqueid){
        return "<VerificationAppRequest>\n" +
                "  <Base64Image> "+imgdata+
                "  </Base64Image>\n" +
                "  <UniqueID>"+uniqueid+"</UniqueID>\n" +
                "</VerificationAppRequest>";
    }
}
