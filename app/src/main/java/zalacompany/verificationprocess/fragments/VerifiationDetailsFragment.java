package zalacompany.verificationprocess.fragments;

import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import zalacompany.verificationprocess.R;
import zalacompany.verificationprocess.utils.PrefSettings;
import zalacompany.verificationprocess.utils.UtilHelper;
import zalacompany.verificationprocess.webservice_pojomodel.VerificationAppResponse;

/**
 * Created by optimtec006 on 26/03/2017.
 */

public class VerifiationDetailsFragment extends BaseFragment {


    private TextView TV1;
    private TextView TV2;
    private TextView TV3;
    private TextView TV4;
    private TextView TV5;
    private TextView TV6;
    private TextView TV7;
    private TextView TV8;
    private TextView TV9;
    private TextView TV10;
    private TextView TV11;

    private ImageView IV_faceImg;

    private Button BT_next;
    private Button BT_prev;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle saved) {
        View view = inflater.inflate(R.layout.fragment_verificationdetails,container,false);

        bindUI(view);

        Bundle bundle = saved != null? saved : getArguments();
        populateData(bundle);

        return view;
    }

    private void bindUI(View view){
        TV1 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV1);
        TV2 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV2);
        TV3 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV3);
        TV4 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV4);
        TV5 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV5);
        TV6 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV6);
        TV7 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV7);
        TV8 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV8);
        TV9 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV9);
        TV10 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV10);
        TV11 = (TextView)view.findViewById(R.id.fragment_verificationdetails_TV11);

        IV_faceImg = (ImageView)view.findViewById(R.id.fragment_verificationdetails_IV_faceImg);

        BT_prev = (Button)view.findViewById(R.id.fragment_verificationdetails_BT_prev);
        BT_next = (Button)view.findViewById(R.id.fragment_verificationdetails_BT_next);

        BT_next.setOnClickListener(_click);
        BT_prev.setOnClickListener(_click);

    }

    private View.OnClickListener _click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.fragment_verificationdetails_BT_prev)
                getActivity().onBackPressed();
            else if(v.getId() == R.id.fragment_verificationdetails_BT_next)
                getMainActivity().setFragment(new SelfieVerificationFragment(),true);
        }
    };

    private void populateData(Bundle bundle){

        String xml = bundle.getString("VERIFICATION_XML");

        VerificationAppResponse model = gsonXml.fromXml(xml, VerificationAppResponse.class);

        TV1.setText(model.getGivenName());
        TV2.setText(model.getSurName());
        TV3.setText(model.getDOB());
        TV4.setText(model.getAddress1());
        TV5.setText(model.getAddress2());
        TV6.setText(model.getCity());
        TV7.setText(model.getState());
        TV8.setText(model.getSex());
        TV9.setText(model.getPostalCode());
        TV10.setText(model.getDocumentClass());
        TV11.setText(model.getDocumentStatus());

        IV_faceImg.setImageBitmap(UtilHelper.Base64toBitmap(model.getFaceImg()));

    }

    XmlParserCreator parserCreator = new XmlParserCreator() {
        @Override
        public XmlPullParser createParser() {
            try {
                return XmlPullParserFactory.newInstance().newPullParser();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    };

    GsonXml gsonXml = new GsonXmlBuilder()
            .setXmlParserCreator(parserCreator)
            .create();
}
