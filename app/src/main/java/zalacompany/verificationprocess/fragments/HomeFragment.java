package zalacompany.verificationprocess.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import zalacompany.verificationprocess.MainActivity;
import zalacompany.verificationprocess.R;
import zalacompany.verificationprocess.utils.PrefSettings;
import zalacompany.verificationprocess.utils.UtilHelper;
import zalacompany.verificationprocess.webservice_pojomodel.VerificationAppResponse;


/**
 * Created by optimtec006 on 26/03/2017.
 */

public class HomeFragment extends BaseFragment {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 124;
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;

    private String encodingBase64Img = "";
    private String userChoosenTask = "";
    private boolean permissionResult = false;

    private Button BT_takeImage;
    private Button BT_fromGallery;
    private ImageView IV_preview;
    private Button BT_next;

    private ProgressDialog progressDialog;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        bindUI(view);
        return view;
    }

    private void bindUI(View view){

        permissionResult = checkPermission(getActivity());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Processing your request...");
        progressDialog.setCanceledOnTouchOutside(false);

        BT_fromGallery = (Button)view.findViewById(R.id.fragment_vp_home_BT_fromGallery);
        BT_takeImage = (Button)view.findViewById(R.id.fragment_vp_home_BT_takePhoto);
        BT_next = (Button)view.findViewById(R.id.fragment_vp_home_BT_next);

        IV_preview = (ImageView)view.findViewById(R.id.fragment_vp_home_IV_preview);

        BT_fromGallery.setOnClickListener(_click);
        BT_takeImage.setOnClickListener(_click);
        BT_next.setOnClickListener(_click);
    }

    private View.OnClickListener _click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if(id == R.id.fragment_vp_home_BT_fromGallery)
                galleryIntent();
            else if(id == R.id.fragment_vp_home_BT_takePhoto){
                cameraIntent();
            }
            else if(id == R.id.fragment_vp_home_BT_next)
                if(!encodingBase64Img.equals(""))
                    new UploadImageAsync().execute(encodingBase64Img);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                userChoosenTask = "Choose from Library";
                if(permissionResult)
                    onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_CAMERA) {
                userChoosenTask="Take Photo";
                if(permissionResult)
                    onCaptureImageResult(data);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE|MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else
                    //code for deny
                break;
        }
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodingBase64Img = UtilHelper.encodeImage(bm);
                Log.v("RESPONSE",encodingBase64Img);
                UtilHelper.appendLog(encodingBase64Img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        IV_preview.setImageBitmap(bm);
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            ExifInterface exif=new ExifInterface(destination.toString());

            Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
            if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")){
                thumbnail= rotate(thumbnail, 90);
            } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")){
                thumbnail= rotate(thumbnail, 270);
            } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")){
                thumbnail= rotate(thumbnail, 180);
            } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")){
                thumbnail= rotate(thumbnail, 90);
            }
            encodingBase64Img = UtilHelper.encodeImage(thumbnail);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        IV_preview.setImageBitmap(thumbnail);
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    // Check for camera permission in MashMallow
    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            }
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                }
                return false;
            }else {
                return true;
            }
        } else {
            return true;
        }
    }


    private class UploadImageAsync extends AsyncTask<String,Void,String>{
        String xml;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://zala-kyc.com/api/VerificationApp/UploadImage");
            httppost.addHeader("Authorization","OQ3tQXnKL2jYbWkkeu9yCg==");

            try {
                StringEntity se = new StringEntity(xmlString(params[0]), HTTP.UTF_8);
                se.setContentType("text/xml");

                httppost.setEntity(se);

                HttpResponse httpresponse = httpclient.execute(httppost);
                HttpEntity resEntity = httpresponse.getEntity();
                InputStream instream = resEntity.getContent();
                xml = readStream(instream);
                Log.v("RESPONSE", xml);

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
                catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }
            return xml;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();
            final Activity activity = MainActivity.wrActivity.get();
            if (activity != null && !activity.isFinishing()) {
            if(xml != null) {
                VerificationAppResponse model = gsonXml.fromXml(xml, VerificationAppResponse.class);
                if (!model.getIsError()) {
                    Log.v("RESPONSE", model.getUniqueID());
                    PrefSettings pf = new PrefSettings(getActivity());
                    pf.setUniqueID(model.getUniqueID());

                    Bundle bundle = new Bundle();
                    bundle.putString("VERIFICATION_XML", xml);
                    getMainActivity().setFragment(new VerifiationDetailsFragment(), bundle, true);
                } else {
                    Toast.makeText(getActivity(), "Image not verified due to "+model.getMessage(), Toast.LENGTH_LONG).show();
                }
                }
            }
        }

    }

    // Converting InputStream to String

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }


    XmlParserCreator parserCreator = new XmlParserCreator() {
        @Override
        public XmlPullParser createParser() {
            try {
                return XmlPullParserFactory.newInstance().newPullParser();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    };

    GsonXml gsonXml = new GsonXmlBuilder()
            .setXmlParserCreator(parserCreator)
            .create();

    private String xmlString(String data){
        return "<VerificationAppRequest>\n" +
                "  <Base64Image>\n" + data +
                "  </Base64Image>\n" +
                "</VerificationAppRequest>";
    }

    private String testXML(){
        return "<?xml version=\"1.0\" encoding=\"utf-16\"?>\n" +
                "<VerificationAppResponse>\n" +
                "    <IsError>false</IsError>\n" +
                "    <UniqueID>ID-66684471029</UniqueID>\n" +
                "    <IsMatch>false</IsMatch>\n" +
                "    <GivenName>LICENSE</GivenName>\n" +
                "    <SurName>SAMPLE</SurName>\n" +
                "    <DOB>5/5/1966</DOB>\n" +
                "    <Address1>123 ABC AVE \u2028ANYTOWN NY \u202812345</Address1>\n" +
                "    <Address2>123 ABC AVE</Address2>\n" +
                "    <City>ANYTOWN</City>\n" +
                "    <State>NY</State>\n" +
                "    <Sex>M</Sex>\n" +
                "    <PostalCode>12345</PostalCode>\n" +
                "    <FaceImg>/9j/4AAQSkZJRgABAQEBYgFiAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAGzAVwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDda3gIKsCVPbe3+NJ9lhLhijDH/TRv8anGACQuVPamnbuy6naenNc12K7ITZwBm2RsAeTiRuf1pDbWrKAI2OPWVuP1qxsRlLEkVIqJFxlzn0FVqPUprb2wbJHXv5rH+tH2O3ClPKYKTn/WN/jVwxrjLbzn2pDAu0gFgTRqGpUNrbngRtkD++3+NJ9mgxlU4PX52/xq55SbmBDZxSeSoIK59MGjUNSn9ktzj92Tg/8APRv8akFrbh9xiII6HzD/AI1ZMSHcvzHIFKsQGVVScDNGoalIWdsqMvln5uSPMbn9aDaQEruQhlHAMjcfrVxI127tp3GgxhGUSDr70ahqVBaWpyTCTn/po3+NMNpbBcbfl7L5jcfrV8Kpyqjp6U0pERu2Ngds9aNQ1K/2WxaJWaM7sdfNcf1pqWOmhWD2+c/9NG/xq5tDIAEO0fw56Uvlqx4UIR6k09R6lKKxsHbLW33c4y7f405rGx3nFuSG4P7xv8atMoLAHj3poKJnAzg0agUrjSdL2KsdqxTPI89x/Wmy6RozR7jZfMvT97J/jV7K44yQxxg+tPBKkfKDjijUDLXQtGdObBtx7C4l/wDiqE0PRmIZ9PcuvQmeXj/x6tVScs4ZQwPSgMcnkLt5J9RT1Ay10PRzNIJLNiSef38o/wDZqQ6Dou4qLBipOeLiX/4qtQlgAWXls85pQ+E3cA0XYWM5tG0wbNtpIv8Auzy//FU46Vp6qdttIpPbzpP/AIqrpZ41IPXqDinK4BLq+4nsRRqBUj0/TgmDa5YdD5jH+tC2FmNxe2LE8Eb3P9at75CwVdnzdTSLtaUgqRt7k9aNQKq2Vu2VNrJ5fYb3/wAaV7G0ZkdbZ8jg/vXH9asgsJWPC9cc0m5TztO7PWjUNSu9hZKh8u2IJPOHb/GlbTrLaCbf5sfL+8b/ABqwJGUMQOCKbnABx8x9qA1IEsrPad1uwbP/AD0f+QNP/s2xG4G3ZUHIPmN1/OpB3Unk8mnEAsVLNgHjGOtGoFZrC1Lh/JwD1HnN/jQ9hbGMqlp8x5zvbGPzqcMrFMKVwp5JpeXXaG6qf50BcibTbNiuYFJXpmVv8aYun2jAP5OZDwQZW5/Wp2VQS2SSKbsAOQOT0+tAEI0+xA2tb5GeR5rcfrStp9sJtyQxAqOnmN/jU6ttcE4z0NHlZIBJBj5Pv/nFAFcafbAf6pTuY5G9uf1pTp1q20G3AH++f8asbpDscoOpI60qtlyCDt6nFAESaVakuGgO/HXzXwR9M0+HTLJYwDbE4/6aN/jVgAMM5IqVGO0c0AUD7UBlBO4fSkyw7A0uOSaxJHw4wy9yeKeTnnOM1HH34zTx1JByMVpEoXaRzntSbSQMHB6UMSGG44BH+FBAI+8aoBSANuRtOenrRKwGMKVfNKcAn5t22hwwIzzxkg0AMAbdvJLeo9KCAXDICuOtPA3dBg49aTcvHI9KAGMGEfXOe1KA4wQCMdacTwenHTikBfYMnG7rQADHXH47sUhbB+RsE89c0pVASrn5dpx3waaNyu2drYGASKAFyr5Kg7geTSFctljzSnA5+7nnjvSN8yj1zQArswXYSTuBx8tN2leMjoO2KVww28ZGT3pu8FD2A4oAXg9QDjmmkjBfG0HoSageRBIy7jgLnriqlxqEIhMfnMSgz0/Sgdi211Eshi2tK56BOhprM7YLJJEem3PI+tZy6hbJGZi21h0OOarXWrS3ABtJmAHOSOT+dOzA1zN5CyPLcphcEYbJqv8A23arEcSSO3m4IHBrDD3DyLMJWmMh+eMtnj6VGMu8reWEG4k7hjBqlEGdBFq/mlSAkahvuySD/CtFJ4pRxLGWdeORXIErFGQQW4zxzViCZoriJpJpU+Utt3HPsMUOII6pBtVQr8ZGcUq4I+VtozwDWdHqlotspEx3jAKkck96YurwH5fMc/Nj5U9amwjVwN2TyzDilCkIMg5J5qKCaKaMGNtwzkdsZzipA/OwSEuO3tSsArAA53cHtQNxfBBYdjSnDEMQQO1AygYDj3pgISD+7UhTn7xpQQSI1A45C+ppAFAzjrSsxzlV+boMCgVwXKgZx0ORUbhSMhuRzinZwhIOfUE0gCtsG0LkZzSGKHJVSQPp3pwYENtG09ielMGSFLA53DJx1FOIQlgxOAc8/pQAFlJwe3JA7mkEiOokKELtxhhyDnrSqC03IGFU4Pr6URn5QMDnigBWOZArMGPQntSqCGZsjYOMU0YKLkKG705clVOR0xwaBEsYRlwVwPpTwnvTNxXHQ08IcUDKIQde9Lj3pCN33TS855FYALESCcH5cc1KixnavLDn+lRxcCTnp1FPXlAVBUdPWtYgAGMDH3R6U8B+5G36VFtdRjJIbvTsAjkmqGIzkLjaM96Cw3qSOWBBGelKjAZ3LTlYBc4yTQIaCMMR1GKRuF3KBj0pQpYMygADqD3pqkMTxx9aAHBW3AdAfWmvuDgdvYUAqqsH69ifrTGmRJVViSPUUAO65GCfTIpjgEM7HGAOB+X+frQZlZm+YqMcZFDSLldoCuB97IoAVSdqg7Rxxk0jSCOYIwPI4IqCaTKum5C3OQWGc+tVG1IRjE7OpH3QOc0DNLeoAZW2nnrjFU7nVEtmMYXfLjsRis6+uriaAOxMcPJ2lhk+nSs9plEiKoOHGeBnNUkCJbm+nuHZnZUZlxjb0xVRZWAYqTkY6pSMB8vmLuHJAJ6YxzSF49xMbOD3DZGPwqrBcVt8rCRym3GWJ+Uj2x/9emLcEjckRBPAI5zTnmVj0BUdWb+I+tCy/MSzk842jjFMQnzBRhQN/cHp+PanrIUUR5DAEliT7VCQ6L5qAtuP3T+vNNcmTgQkb+GII2g0ATSFd7BgzLjGU7dP8akVykjOyk7SD749PrVYs4lk2sQuCpAPrj/69P8AM3O21GAdPl5oAmCHzPPViSzZClc44p1vcOrYUAA8kHv2qCEHy1kw4KMR8vPt2pokZE2OCw45JB9f/rUAaFtO9vuw2RyxGAcYyRW/pN+LuOMMq5wedwHSuVMqmU+VGy7RzyMGljllDs2CsYUjapGQaGCO3GWQdcDuDSLlieTkcnIrnrTXFgUR3KySBjwycgfpW8lzbzFEWUFlGQMjP4is7APxvywOB0xilLbGYEH5RnIFMOHySrZHQg09NxcnLDdgc0kBGyLtYHdtPU4puVDpGNwQKOD37U+XMkWI3Gc9T0pjIGhA8wF0ODigBwJKhj0wVApWQEtjPOM59sUbihby8uT3x0pxKuVy2WHUjuaAA43KTnJAAGaZuUEEDOKk24BJ6imE5UbcH1PSgB5AYZYdKSKNQd3RQDz70EAqSxDDsQaIy2dpOUJ6UATxnCZbnNLhv7wpEUBiMc9evSn8+lJgUvlHalHQkc0uCOoFNO3+Ic1gMfEFJkJwDgcetSO3lh/3e0Z7D2qKM7ty9MjNSR5CbsnO3oK2jsApAYKrDAABH6Un3VYY/GlY7tgJ/h5pq8MAOBnk1QAz4J4BpFO4kADj1pQpOSStHLsVyARgn3oEJlsj+77U05KH5gp+tBKswIbBXimkF2IcHgZBx3oGGN2PP5AzjB68ikIt9xJCjB7mnSebkCQqV/2RVK6EbMV8stJjhs8CgCWe5tEIElxHE2OAe/6VWnvYUjaQs7Njho4yarzS2luRJIpd9h6OBisia4aZ8tGFUYHHocf4U0Bbe93OGjmwWPIZeetQ3F15uHkBR0HGVwMVTLxh0OwlQ5HJ+lR7h5cgZRgJkbTVpATyXHnR7FmkbauSO2aiDLhXLbfLFPkbzXY53swwB0x+VQsxiXbj5BzIFGe/FMQ+RnCy4UswGVZhkYxzj3zTdwAGIi5cAHAyx6U8uUmaeRFkmRMkZIzjke//AOumLdnyY4I96vCMhHAxk/rQARhWkOR5QT+E8Emm+YUHmFdxPYDkD1/Sk+0bgC6OXLFWIHy5pTG8GIyvmFeCy9DnmgB7SJFGIkDSr2GeVJ59qa5jyhjLohA3gtwD3pMsEKrhCevfNO/fR2rLlMMrDOOeaAGsYyDskBOODnrTEWQMFzkR9cP2/Op0i2gKoDNkHkketQwyr5Yl8s7N2CPXkigB+XTCwEjceQzcHPXHNOkdC5WWIR4GOO/6mklQyYcrkcbR6e9PwwuPJWXzXIBMi4xj0/DNADEEkZZMQng8k80L5jks+VVuMLSSiPazDeVVsBfXtQhIjDlWJHTjtQBKWkVwAFG7oMc1PDez28onhmRVQ4Yyty3bGDVcNKAFVSzN1JH+FOWUxMo3RFV/76pDOt02/jvbeNjIodhwFGMmrOGU/fII5OT0ri1llinS5jVgysCAygg56iuj0nUftsTxyxqjxk4+brmpsI1AsbJtII7+maaFRUkAjC46dz2oG1WAPHyj+nvSyYwrH516cdqkBHkdMYwueCSKTMcbcKQBzyKR7cNtjILBMkGpCzBACBgqR/KgAwWUAN8zcjJ6ikAY8ExcelMwmBlSc8Hnt3p8JKBkTaE9+aAFGwjpgD06UgYAY569qQBctg9AKVNhk5BHOevtQBYyPXHPXNOAGPvNTABgnqDUyMNg4pAZ+V+tKABwcGgEAbiM0nBrAoei7pNoOBinFmLHbkNjrmmJtD554H+NPJI5UZraOwhxIGT97AwTTCDtODwecU5sEdMHvTCRuDevaqEOMeBxgfhTDhPLJTIbJzn0pV5PG459aCS2B6dB6DvQDGS5MTsvHOQPanowwDjPGPxqN924MOTnGPWq11NcKipDsVs5fPUCgYl3dRktDE7h5TwBxgDrzVK8mNrCqmNi23fnd2z/APrpk97FaxsttIrXPQMV4HryayZZw7ytcNly35g+n41VgHTNIz/MFLcZz6flVdbjaxkZtzfKdvqRTcvv/dhDIV29O/anEtJGCjgTEk4Ax0P0qhDZXeUSOuIuNwzg80scyMCMZ4wTgDNMLOZGMwDYwPm9Kf5sgO04aPsyAcUwIcEkhptoOCPlzU0SvEHYMvAHfAPuRUIbDuoTaG5yo60SnZlQRyu3H1H/ANegCYA+ZJbOVkBYHp/DgZ5xnrUO93faJDuD555/CpHPl3DBGBwoyVHQ+9QRAqEYq2Q2TIBxQBYXLOWaU/LkFcYH6cU6OHZE0CP5ZBzksSB+FRiZ2JTO9GPPHNRMCJdoby127m3dSfagCRZEfCgZfPzkE80rKNm4cYJX17EConuJCUDEgckrgZA96QgMm5FKZbPmDmgCWOVXVXdN7Mdpbgc9uKdGxVMTKSEC4GB1NMlQFikDMVVd3zHvTTGkkZGGMiqNw3kDIxxzQBKESM7nRiCpIAbGMmmxQxxSyByCw6YXGPSoGWRZS7F1wuAM9v5VPtVZP3ZIjLZLFuvAoAAwjzhM5XIwB97saejSmMtI7dsZAHpQJJHYeUx2M+eOuKV2+VwXG8Z+WRj+FAAEkZo5DMFLDIAYnGDSsrRShHIzJyCo49aRh8z/AGY7VYbflxjHf+dR7TvRGXbAyjblTkYUUDLEAMksR38j0HualSCcDcsRO05D7wMVUI4VSVITK9j75pUAQbgFUgj6/WgLHZWU7S243SMSEGSan2q0RCkhj1OTWLodztn8l5lZmThcj/PetvJAGD19aze4MQkYG2Tnv70FlbgKQRk5PPalIMiHAHB5pTkNn5Qdhx+lIQijaFYP1AyMe9PYY+YKAPWmruMYYMNxwp/Ok+ZiVHG3nJ6GgBQVVy/8PsKN4BZyu4McDpxSB3BU9B6Yo3OdyNkhmz16UAWVwgZGySB1xT8EfdZgPpUWOeACrKe/elZYyc7W/wC+jQBB04pBnPApefWkxXOXYcpIJye1KxwPlPXqTTRIokUAdakbOMHpmto7EsaCem0jPcjrRjDcjPoaeSxPTIUUikFcouH+tUIbtZcYwfpTQcFinft3p5wO1Nc7cgjG30oAY6yFDgKQwOSc5FZd5ceWvzHywoJD+vHTmrlzJHyoLB2UkY71zl9eEbRLGY2CEAqd2PemlcaK5Jbc7A5buep/CoJ4w4LsSccj36U8q2BL5xGFJ3YOenpUZlYqigkgHO7198VoIdCnmTb1IUAjPfApZGKIioB8xYAnnHPWmzSYcZYk+uMU1pA6ElW+bgf5/CgAjkjTiYGR85IVscUKlqm6QvJGp6Hgg/iaIGKoqLCp+VvmyOKi4WMRRsSApLIV4IoAkEoJ8wKdrb8seg9M1EHU+YzD5yuVb+Ef5xRI8Uhd44jtJ2mMnAXmmt8sEhVgwUAbfTJ5oAJG2BnkV9xHzEdD6Ypm8JCshibYCMKzYPtT41ZSEceWwwWfOcgHp+tHlJDJ5kmJBznGRj0P8qACWVVaOQqSZOQq9RzSsI/NmGJXwoACjp706MEkyxEFQOx3Y/76+tEM8keHuPmQr/dA5z6CgBsCIjMpHyuAfMz8uegP0GakMnlEAMH3dChyMYwP5CoAJJER1wDIGXaf4cn/AOtUoBSCONW+ZSA5Hbr/APWoAQ/M0oGDle5x2NKXEluHe3c4O4q2QCfQetKuTK0X3hgcmkaN9mP4vvj2xz/WgAZskqVxlRtX056fzp0xiaQhItuzPG455x1qNyrRKJSWLMuD/c/z1/GnrICXIJkVSMM3BJ6kUAAfCZVhJt+6qAGmnE6MHxubs/AFPCFmUCJcdcZo83YjRlAvbcOcUAOZhK0coViVU/Ki98//AFqc8TNGJQFTaxILNkY59qjEcqqFL8bSM+5PH9aRQYnd0kKllAHHQUASrPJHtljcEDO4oMg9sZP4VOZJ5ufMU7uAcj8ulV45ZGJUXJJVS7qV445pvnJ5IOR+8fMZ2+2P6UAWbRvIkEsOSx4OV/lXU6ZdebZRl0O7pjp0rlMsBGhQP75qazna2ulkUEITgqG6GpauM7NeN2Mge9OIAZSWHTGM1FBKsybgSMEjpSiVXEeHLEjABHQ/5FQFgLoA0YDNls5XnBp4J5GeR27mmFsDlcnuB604YXJI2sRxQIAWwvbPr2p4cmTaSgOcn6UxDuXDNubGKcq4jPABU/XNAEkZJXocfzqXA9TTcsJGLD7wp+T60DKQB9KDuC527v1pcKO1GAOlc5QKXyDsA/CpCCwAx0qNCS+MkYNSZJUsD93171rDYlgFbPy80BAQq5wT70AkHg9aCCpAbnHOasQwqpXMf3c4pWx80jjnpj0pSYyRtVlXOcZpkmRG7YHzcUAZN9PIssuRkbcjjFc/JK5BdQFyNpBHatbWtwLKDE25ejnBXpWOUcodxG0A9DmrQxmxCzCR9m5ABt4Pb0pmxQHCIx9Cx5p0mEi3ZO9QORUZbKLKQc9/eqEChUAEykk8DnPNAG1gQsxYdl5wMUf689RGEG/BHX2/SnRr5zZX5S44DnpTERKyfKqhS7ZB3ZyO1SQIMKJDJGRnB3cGgq1rP5R3MG7qOOff8atC2VQR54AXoCwqWy0iqu6TAMe5s7c46j1qOVMMybW6YIz7VdWN/MRQ2CAMntT/ALOCCSSzdOKnmsVymfNGWdiGCsyYAJ6k/wD6qDbmQ/uYEeRsB8tnH05q+bSI/MxxIOmT6U2OziJjXGSDnKk/1o5g5CogEbJE6HcdwYDp3pnkK0CJEvygHg8ZOTya1VtUjkVvNOFyPfn/APXSpap5pV2OcYyKXOPkM0xOjMdgR+NrA9OKfbwsA6k/O+SXDf59K0vILEuRnZ7nNMZPmVsHrS5x8hm+SHBd4sDG0MOTmhFnIPl+azpwPM54rR8hI1zGCCPmx1qKWCJgiMN+fQ4I4701IHAq/P5SrKQGAIywHBzng/SkVCDI0ew7gNokbIJxjgVJIirEi/8ALFGztH1Pf8agZo0ZXUOAMKpJBxnn0+lWmQ4jWaMxQ7pSr7v3g3H8qVi4Z3jjHkEfe4JP4Z/pSSzuYpcSb/LIDjAHOfakhbIRlCgjnocUyB8hLFirBYz0B+XmnKZw6Y4UDgk5piPJKHLxghRjk9DSW8kavlThlHzMw4z+dAE0VxukeF7lhJg5UKRxj6U0TN5AR5yFEilAo7YOR06ZpVJlLlpU6MQc4B46VGGFuiK4aTdwpUD5eepoAsgSybPLKswNS5CvsuEUKThm/h9DVQrHbgKCzSOevGBSlfkkiZC6FMnHvzQB1+kRxW8LptJPOCOQKulx5a4YbgOoFcrpGqvAQH2rG56AYI5HrXVqTJGvkqMEZOKhoGIMBW+XJOeRxmlVlCB3bJztUDtSYIAJ4YHNBAAwq855qQHNuDksQVHUg96QAY3ZAyM49KfGF3MnTaR16HrSBS8hYDHJHFAE6nLfOTmnnJORUW8uQWPpUoyB0X8jQBUPPal2+9GQKTGTXOaCYBlXjipkwxJAx6VGoAkXIK8dxTvu9QeAa1gSxf3m0Ade/OKTaWyB/wACyetK2EAbJPHSmhYywbPX3xViEKh3UDmlmRzGB9cYpwkOTvO3021DK48l2b7wUkYPP5UgOa1iWR9QkRXGU+U7kBqg8WZORkY4wcc1LqLo97cu7gEN1YdaqSIsrruCgoR83IIrVACg+Wrs5bfnIxyADjmmZjXILMoDccVICwSQo7bZCMZHI6k/Smo2wKowd5I5HPXOf1/SmIYxRXbOHIXIx0/x/Sm5UqGLkcjJ25PSpgpRjF5pkKIRxz15/rS2wIZ94YKQAexpDRJbxMYMopYdQxOP0zUzxLna+3zG6fJRHErrtjUNGTg9T+tTxoycNx/dXqazkzZLQdHbFvlZRluDzUwhjRyEYIEAzx1PNPSPbH0BcexPFObaY9qhFZgOgxWN7lkLIUJZm3H1HWgoBgKiqzc7s1NMHjf92nXjKjIxTWUrjOOOvtRcCNokCg4BckZp7RMSW281IgikO0tjBH0qJhICHRjjvzQVoNVB5bs0e1hjnNBEm1SM4PUU7A3HdyT71KvkvEu8sr9sGmJaFRkzyWG8dqZNEDkkbMnqKmMse8MwB5Az15+tJKIE7B/xzTuIquWQKN3ykAYxwe1QXCAFQyqFJBGOuasGVPN5bAJAVMcA56fpTDHkBWUh8/N1q7kMpCOdZpFdFkJPLEgUjSPEjB2UIOqf5FWZAjj5CD8w3Fm61GsUays5RmBGMKK0TIaKx2QzZdC7EdQfTpTbe5Lfu2kL7ly6kY68496sR2iyMcCV3/E8f5FRQxRxRxZRgRtZty9gPWqIGR4F0g2BEwzLnnkA8EVOARGp28yAj7xAx0+lMjeNLtmWMeUUYkYyfwFSCFHRy8JZiMLuOMDnOOlAhpEkMKyMgJPJ7mnbpDaxMjGNS+eT2z7VGhQs7KWVl5+b7q47GgSEyZDKwbk4IIB70AXA6GVm+VwCAcdM9f8ADtXVwMWfCOUXAP4elchGYTE4EayOOcg+31ro9Jud0KxyLyUHCrz3qZDNLgk4bOfWn5CEb84PpTMrlBGBz69R1pzoZY8TDlTkYNSIMAuw9CD83pQmBIOoG4/qKC4I5747daUMjHjOFIHT2pAWFwfkVcbcc1MVzzUQztyGyTUwZMcg/lSYFJuTmkBpRu9ab3+9WBoBBaRST901YKeY2X5HtVc53IM9RT8EKRmtYbEscAvQKffNIcCRMjPLUpJIUjjA596TPzA9u1WIcEaI/uwAp7HNVrqaQQOqnGEbPy1LxnLcEelVZwsqybVySpzzQI5S7lZ7qUOxA3EnKgZxVdpJDscDO47vyqzeBI7q4Ykp5b/dBOGzULclS3Oz5se3pWoDVkWOUmSPzG+91xjP/wCukIUTF3jO4nKAN0qJCpUFWy0nr6UmS15AAVHUYA4NAEvzRuCVGTjOKu/Z18wMj7c/NyarRrtVjLgrkZwOlWkj3MVCBl3AITWbZrFIkeMbMynzTjI28YqaP/V8/wA6PKlQx5jGMDjNObLMOFQDvxWTZrsLCoVzjIB71OrM3AkyAcdKbEqs5G4YcjkCpcKXCgZ5wMGs2wIWKIw3quCQMscCmvlVcLv2g4JQA8VYEe2FRMAeSQaiYBkceXkYJPzU0CI03FlUBxGBjJWgkiPCA4B71IseVUlM4x1P0pPlLsWcjPRaYxJFO1vwpiyBEw0RcJz1xUrKVDhVOR71GwLzCIDGV+bPPahCZXL+XlU/dg/MARnJqV23uhaAcNncCad5bxlVMuNo6D69KRYcqZF2joeBz0NMRH9qcxyRKFYZ3DPX+dMiuJIQZIsKX6hxnpTpFLMpU/vAPvGm7ZHZySCwAB5poZFNK8rFxIokQ7gFGeacbt2hVpJoiygbkb5Tn8DUZcSOwULgAlhsGTj8Px/CkfLICg2rjt3NWTewyUQXcaTTStbqXCFWTkAd+T7mn32jxWm1ob21lV22IYSS+BxyMkUwq5QfIC5/1vPX/IqJo1Vssu0FhyBzj61aIepEQI5zE5Ej4wSeCwPGMfjTI2j2GKQBWU8IeMe/r3q3BJJbzvJCQ0W3jOMqfXmo5dVlkRTf3AuByFJGGB/Ac/iasz2K8M6vNcKkiRbScl+QevvU0TSLby75IjCQcMrYJPHtTFklmEipPuiwCcjBHH680hMcaozMJX4Cps4/XigRZhZ7hYhHtjKMMnO7cMmtzRo2R0mWQM4ySMdsn/61c3J56lWdiOemeldBoTJLJuUttVM4PXOf/wBdSxm5uZmL7Rk9qkJ2u4Py5AqASYhO4gEsBwKeoYDCKHI6mpEKTh1G7OzpTiWJ8wOMheFHU01CpbcuMnrTlRkBbKBmbjjnFAE6uMAg8HpVgMMdf0qCNlKhCcFD24zmpwQBzxSAonkg0EsKBzQRgYzXOaAM7wT6U5SSvo3II9qaAAygHJqRh82COQMnFaw2JkBGAMkD6U0As4kxwvQUoIZd/Xjjmkbcu1lKgjnBNWSIuXALjqD7UzBMYVAq4U5J704By6ksCcdAOKbcjELZGcKeF5NAHJagMajeK4Qhm+X29azhMUlDEkhcE+uKvajEWv5Tgj5uM8fnVC6cRqCVDHOPlOa1Q2SMwMe1GYLHwokwOtIkaqsXI8wEncOnJ9aSWPeMkqQxBxyCamUMkW0AMuB1PIoYIsJEA5O7dn72GB/lUkJcyFWRxzkEcY+lVo08kBlUkscMoXt61deR42deoUZBxgVizVDkaRiwYlgOFyeTxU42oMBAD39agSVWZcp8+Ad2eAaes22XdxycZNQ0ykyzHh9jDaFXseM5qVd0LiTGQeRjnj/Oaqo5lj3Nx/P8qsxh1ReV2suMelRYdx8aCM/KeCMgnmk2kEMyoR7DrQMxnaQGwCM59aVVAOCQcHI5pjQhG85CqqnHHSozGMKGTp6VYbbyXAGBxg1GgJQfMvy0AMaMedIdsgLD8qBCGXKr85GCSakXEYyuFD9cnk0MisOQxjY8t0xQA0iPcmHyw7CqqxwlFYnO4cY9astt8tssMDpt57VFM58lW4cDqB1FAMrTBlk5GXC/KO3tUW0LgHG4j5if6VYLhGXDDJwSoIPFV2lMkAfG5TnGe2BVJEkKqql95xno3tTGQNGzxFyi/TFOMoIVCVycdDnH1pssgGVZhsHVemauwrjZN7ljCWAK8npmkMjsFVgVVSu44qWTd8p3hdynA/GoXHmIGbHC87TyTj/61UIgeZorjfBNslSQMhGc/XjmrUmtM7SSanbWtzIcBJ/JG4HHcggkfXNVJHyUfy3OzHygdagllLqx8p0jk+UhlO4cc1aM5GhaXlncZh1e0bYuQJII8ODn647elRSx2Ut0E0xpvNlYKgnAGAeKoK3mmSV5NgYksvHuBj1qaNxHcQ7CxLc5xnbxjOPwFMgsmPyXnilwZIWBwec5UHNanh4iK8YfOd8ZHzdeuax5Ha6czTyJ5rYG5MAcdB/Wui0uICVZN4LqmcryDz0pMaNcAEMA43H3707EpXeWiGP7pxmowE85ODk8kjkUmwFwVDDPXIqAJQFGT6c4HNLwoJUP2IyOB0poYBmkLDDHGPangjLKHOwjPIxQBZRw4yVGalCgjnmok+YkggqPTmplLEfdNJgUvmHaincg8nNJiuc0GkjeBsz71LjG5eR65qJid2MjFSEbgc5A961hsTIYUYbVXoppxyGGVy2OKcSo2kfeOefwphVvLJL5J6nPSrJHMcMOgpiqjI5LZHpz605yhO9hx2zTG4xsKmgDmvEcKLqoEUWdyhnycelY86lDvQhUVwzrjqMcit3xLDm5EzRjPljoeODWFKWLEsRjIACcYrRDYXMSPuaBfLQEFGznmpLWIhdxPOeTUZLFSkZ3DOF78d/0q3biYCMRozZ4K9ulDY4omnaRFXbLtGPTryaieaKSSX92yEqRyc5NPmidv3e446gEZqPY5IaQFi/Hpjjr2qVYt3I2+ULtz8wUA/jilaXy2UMx++D0qSO3RY02qyqSSfmOQPao2s3hVmB2I3fdyfrRdCsxFuTsWRVdQ3ygk1L9sZFCFmfIwPaqv2Z4UJiKsWGAA27H4VGqMhCYbd94grjFFkF2a632xUVmfBOMe5q0k/zOq5wMc1iRkKrh5CrAZRe1TJch44j/ABAjcdvas3EtSN0SArhgW+UU1pI02sxMYb7p25zVO3dHkDDJ2LnkeoNS5LRlVB3A7hkcDisiyxNKIwpUMpII4/i/wpqt+53bwGbjaBz/AIVG8kgRROVBV1P15qvLcSKZDEyhdx4x7UCuTCdoSFeVxjqAOtUrq72tM4JjaboAKrPMPKXqrjqAPx/rVSWeSRtsrl3JG7yx8w+prVRIci6bqRmA3YXaMnAqpHKrQIkRwdxPzHr2P86jlhJj2xvhV5Jb8/zoDF1yPPKDpImetaWRF2P+0BpfKWZkcsGI28EVL9oAili9TyCM5pZGmlJLsWDD5XbrTHR1t8RyyFic4Zvl96egagsqRlmkJbLBYxjOPWn43FgFQY9fwHpUaxuWbYpZc5yvTtUlvAqLHJKoKgUAI0TGPepQ7mKgdOn4e1VZpfm3PECQMHnv19K1XEctoZduEUkYrMMmIYychg/THOKezE9iFJFLq5YA+WU5GeSMCniRRayRMfmYndIB29P0pnzld0RQiJSq+ZzweDTYQ80qxNkxtknn5eM9PbimQTAo5WVFRIuwI5Pv/P8AKul8JTmOZknnaW1eMjYRwvzE5FczHLlQWKqCcAAYrpNAiaC8d/LPl7e/Q80mM3XkUuwiPygko/oPSmtIwWMHLAk81KTbkLJtkjbqSBzjvxzTZAmF2S7gVPDHB/kKhgIwIX7pIPA6cU1JjG4gDk5XJU/WnbgRn5hzg464qQKH27clNwzv7GkBLCFYsyrt6cCpyQcEA/nUeAWLOAQCMYqXbkA5PPvQwsU8D8aME9TTtvJ4zQMdziuY1GFTleRzUxVWOM8D9aifO5Dj61M/ysAV4HINbQIkNDZOPypp+VWwcA9QO9OJBTjr34oJKNkqNgGc5zVkkfljgnmquozCC3LbeN2BzV2MAq2GyazNZXdbQbjjEhHTrSZUVdmRqSRzKJsHO0An054rNuIUd2O5jsI5PfjNa7CTyCABt+vSs2ZS0wLNlFGc9OauLuXONiuiLuU7NoBwGAOcH8a04gQka4JHqe9V9MSREclyS/3efc1bljkUku3Rc0pMIoSZcSKSCxHQVBO20B1G11OF7/56VaZmTBRg3PORiqUpkjlJfbuP3c1C1KY17zeQx+Xy0wQQKjjuMDPzcrnO3uKmihaeZXmVSvGdvp+NWtSGJYltn+TYwweDnP4etUrE67me04/dM3y+aXA46Yp0cgxlurDGSevvUd2ZXhjjuGG5gdoXr+JzV6ysIBbRfaH27l643FePpVMSuyqwyA20kJxxz1//AFU9c/fO5UYdCvIpzWgWQ7JyQBwRlSaVJGeQFHZnzjBPBqWWkWLViSADwygH/a4PWri+aIF3xbSRtO3PI59aqRkQ/Pt5/iJOO5FXUKpGuCx3njn0rGW5otiLydjMEjxvcAMe2OhqC63bJEJJ+YA9OT61YkwjTBnYZOc9cGqUuMSSZYqo+9n7x6f1pokplcMxMmCvOT34/wDrU3zISitg9cMoI5qQvJGBlV2t1zyR+tCRrGyxoAXJ5zwBWxmRxERnzIudpzg9hUzTqsqSvMqAnAyR69/0/Wrdvo094rNNtSPkhkwf51Ru4hpsq+ekcgJB+cZ7egBpoNUJ5jBpJFi3ZPOM7SKFYSN98KByVyOtQSW/2m6V2mIWWTKhCQMfQ9Ksy6WI4RcpIFUcEFQd38qGhXYqpDcyb3A+U9zU8rKVUBtyLnbg/pVdMxoQvJbru6U9Sm7AVdy4P3etW1ZAtSeLL2pCgAZ9elVriKUqzOylwNpyDgDGc1YiiXzVbIXsQB1pbiMrKr53FgQ3uKQ7GMGQpsCo275SAx4FFvHGJSsYVBGTjzSfTnH5054MH91yzN83AXiiN1Usrp06knOD0xQZElqpeBf3TFBlsJn1Peul0aZLnEY/5aLjg5xzXNwx/LGJGZR1UxHrz3rY0cGK7iEilcngr260mykjeWMQudgPGV/OljkRBIWiLEjYOcYzTDw5YlsE09mClZG4BYHGOtSSSSKkc7iNh1IHfApqgYaMuvXdk01QMqOrHjpTimFdSQN3H3e/FAFkExOwVeCKlA3AEnkioVDgYZvUCpABgDd0pMCDj0o2hqQ89TQDgkVzmo04wAc59jUwJPI3E/7RqLptINPYLu3YxWkCJDskLwoOaY25MlfvEfWl6nb0B70YwSF+Ze5rQkP9YSX5914qlqQ2WwkAZsOMZOetXGIC8Nu9qq3+GgTCkHcOKiWxcdzKdWLqoY/OORmqGoQopUGTahHGW+8a0XI8xlAO45GT2xWbOyTSKZU3hSNuDjFENzaexPbI0MCA8ZAwVqQvNsOWVh05FPBCqgC49Kcyl/mG1SB09aJEIQhNx3EHPIx2FVZYyJCzbWA6Eke1XFACszDHaoWg3qAT1qYuxRWV40B3Zz/s1PCZkRIzK6gHJ4G7nOaY1sCXVM/IM8/nTpbbaxkLp8wA61V0NDWSDYg3b5OfvckUx0VZh5aMeOSDwOKnEScHcF+bHHOaPJIc7DjByfejmCxCIg0Y8wlWGRknqPwpNjwsrRqAynt6VLwWJySp4x705kwuHDD3ouFiNHlEowPlfk5we5q9CSXbzFyc8Y6VDBGIiAuSD0LVaCvsbaV8xTz1xis5a7FIrS+afN8zkEjBPIqnKzLEyLIFYckLwDz/APqq/Mdy4YfuyMgr2IqoyCRSygAHjmnEloqnLkMCQT1HWkW3AU5Byxyfmp3lbG+ZCQncU4QgRs4VFKn+JjWlxWE3PbSL5U0ygADbv+U06Z2kBExSUjkbwDilKxyKWfqOhQ8UOmGKHHK9e9O4WuQ25W3xsXbhvmyAQBzU5kWWTaN+373y9KgdBGrpJyp6/lUluAEVlJGVIAq47ksbON52EnA9aVFJO0IBx1Ap0jMxMgXjI/lUyZBO0/KR3obKSFC+XHnjOB6+v0qSFww27Q+QQfxqIEKQw+Zc80Y2MCB82cgVKYMoTK0ckgG9U5xmqjcK5Cqyn19T9a2ZwZ7dVZvlAyB6msp1EciuU3MrbcZ9zitDJrUnhiaNFYqhXsG5wc1paUnl3qqXXnJLcn/PWqaAlCsp6HjFaGmALe8cjHNQ3dmiVka4C42Ec5JzSFCFw4JHbmlXDqyKcHdSSJ8q53Ng9hTOcePu7CG3+1ORQDggkdSWppHzDPQ880qAvL5bfd7GkMswswUgLu96sKuRkqfyqvGCoA9DVrAPIpAU8ZGQKKDn1NH4Vzmo0lsD5akIJByuc1G2SwwKeGIXBOSf0rSBDDPy4xihQc7gobHrRj65PcUoweDuIrQkYDx8jZb34qO8AltmHGV5ANTBRjaqN9cVHIsZjkIXJI5HpSexSMWchd4fKHHG3vWeZGeUZJKgDAzWlMuWcqoOCRisuJSszOw2DjHzUQNm9DRBDvvHy4HenqAwCkgAd/Wo0jKxhwVIPJOaFjZ/MaN1HPTOKiQIJfKSNsu4yeDj2pWQbgS7Z4yfanukiLkKh+nNSISXDFQCeTxUXHYiQbjkHKg4570FY45MSM25uigDFTkeY5P3cDjHFNI+QcZP1oAhfaspV/ujPT1qMZILqTjOKnRcrjB4PJqQJhSzEgdvf3plFeSOPbuVN3qCBinSI3mkkKMr2+lPlIXHzHJBBqJgqsABuB4zUgSRpk4CqOOop8W8cR7Sx53HIzUQDBQcc5/IVYwF2hVyD1xQlqJsguJM+a+OCMAVTyCgypwD9a07qCIcoSOeBnGapyROGPUj2PSr5bC3Ksro2UY7fotOCCRguM471IInAIdXfPcmnQxKWjySqEkVVmOzGASKG2KCvfmmqVEbbVxmrf2Yj/cJ9aX7KN2AOvQjtSGVdq7R8vJHJ49KfCwVQgB2556VJJblSpUhm5/i60sUblsSKVPSqi7CZEIstsCqpPzcfgKYijywkjE8A1bYESZZUV8YyKhc4CqBhV71LuMhQKV2AnvzSFXEYLY3FSDmpMK4OGA/Gq8nzlQv8HSqRLHQyBlKFQSvoapuudQPyqA6kLgc596tBwD0LMepFQ3I3lUba4YZ2sSdvWtlqjOQttvRVj8sRyEYbDZzzWxpcAD5GPlHGe9ZtpGkabQY075BrYsBthI4PA5HfmhJJjbtEufL8wcBGHPrmlOCq4O0Z9KaNrZB4x0zz6UYO4tzkeoFFzAmDqVbP3aQFNytuwM8Ugy2PkABpyLmXZjgHNK4FlQrk81KmFXBGfwqFDjIFSbVbmpYIplhnrThzRjPJNJgBRjNcxsI31xTyME7Rzj1qORV5BPJ6Cn7QNpxtrSBEhS5xn7uPxpDhlzuJ/Khy+75QPxNJjIJZgMdhWhIuSMMrdTjpTQOGwvUcn1pwXrhvl7UzeG+XODH1oGZl1GiXTHlcjO361QNuASzDcw+782Ofp3rT1OINcRyFsEL931rOuC3mlkjBwvXPIqFubLYcoDl9g2nH8Q4NKBldrKMtjo3SoVctb7DJuPckGnICFwWznmiQ0WWPkMFVdwYZqQZLDavRc1CG2c/eHrSpIXkKgn5Rk1kWTfMwViBz2pFXIGTUfl5lyuWwPXFSKu5NxByaAJVZA3B2lO5OQaH3EGRhuHbA60qMxwzgbRwaZIVZwyAtt5ABxVJiILgv5gwPL9zzmk8ncmCSuCCOOuKCEchDkkDjHPJ+tSgAorAbVAPX2//AFUikMjjIVjkcDJqeJMRAIfvd6g8wbzwGLAYxU0cxES5AA56UITHzADYd2SDjFVZV8t3OQQw79qmWUMHyvysODVVyATnJBPSqd3qJEShirAbMfSnxzPgRN/ATjintGpU7cgD2qEvzHIh4J54ppsouryBuOQRRmNIztOPypscgLLhSeOaey/JxzRqIayrhFfnrgjilTgdQfqcUx8MRyOD3qJyd+SoJIxgUJ2eo7Eko85mO35lH0qBnYBQceWASfWlRjGXBdsnoTTZUA38gBhg57cVbSeothg2bWcthT91enOfXNMyoA/hzkZBzShRGQG5Krk/TNQ7RLEUiPy8/MR60khCJ8rfK21s8gmntgfNyWPBII5H5U2MK1wWTbwMHI6USxK0oCk4GCfSto6amMh0YhKeUDNuU5yMHiuijj8uJFDMAV6MK5+HaZAFkwuckAcmt4urNgSEjsCOlT1FIkdVYE5x+tKp8wAjK7eoz1pgZQeOafkMQB+frTIFiwznaD7AmnuNrZPBxTYgrPnGGUfdpfMRiRgg5xzT6CJ48KvGTmrSbSg57VUiLZYBhx9anUllDbhz7GsxlTaw68UYH94UuM9DmlJI681zmoxyoGevbinvtDqMKGGc7qjkI8ttp2nPepeCB8ufU5rSBMhvrkfT0oyGiI/ianZUDndx0A/KlKuuBlMHsOtaEDFIVVC8GhjkgnOT0wOtEiqgC9t3FKd6yHLhQ4I6UDM+/l2ybY1JZQM5FZczMWJI+fjI/Cr9wjNPMCQecZxWdIqocSfgRULc2S0CNjsMTADHJ4pAxJwAOtR+Y2GDN19hTomIfKnBPcj2ptCTLPlll56g9M1LC6mR+zDFQRsCT8wz6mpVDpmQbWrJmhOgUuxIJJpMgIm0EcnNQblBw2QX+bipY5BGpVQTu65pIZMZOAQvQY+YcGomGSWG0Z7LTWVQB32nGKVQZCdy4UdqoLDZEZXBCvxz8o9KpzzyLEBH1BPUev8A+urc6KUUup68YNU5YQJPMAKbj9aasFyqr3rSAsvfjjtWh5+5GHIbd8pA4UY/xqOMsH2qTnvxTZSyqpTAz696b1BWHyTPuZtzHHPA4NQS3BkjDqXyOOB1pWWTYwOBvHJxSRsIdqkZ2nrVRshNj1mlZlAdwBywYYqUhQ4w6NnsTTXjklkOHHA9KI4giq0mVNDRSLkUbIo2sOecA1IqoR1+Yds1GgUgFDlj1PtTmQbQDkhTnioAYpVXI2lie1I5OxgoCt705nODljycgAc/55qKQocs276nikMhZcLuVv3lLlZIcPkuOp96SJmKsRg4H8XWmtMVhJU5D8EYq03sSxuGVdmOcdT6VC+9VVS5PPOOn41OSjRYVHUEc4NV/lV8BZCp6citIoiTJVDRpkoFYjgrUZYDO3O8nmkO0LEiZyw601EJlMRXMa5Oferv0Itdlq3k25wVJB9K3ss8cbMByM5ArnYFzCBnvmtzT2LxhS2Nq/41K3HJaEw8ssEB+anKSDySRnHAqMrhwxOfmqwqnAIUMO/OKZiDM4JG/AzgeuKTBJHGSOSfWmMGBJQgDPTrRnDhurYpMEWoihyDuOe3aplBAwIqrIW2g/KQT0PFWRhRjAP41LGVGHsR9DSj/OaXPsfypK5zQa+SpwB61Ic9MVG4JQ896kVpGBz908VdMmQucNs5JPApjxh0YKApJxnvSln38MoHbNO6KQMcnJzWpAjY6dcHNG3GGzkejc0FtxzkY9qM4GDkA0gM3UImjmefrHgFgOPyrNlcTEIHCtjPzLn+tdGRu6oGGO9QzQpJG42KDg4+uKSRop9Dm2I88CR+Sfk2jHA65/ClKlVd0f5QScEHNPnjCsAoTAOKjla2jj6nzCCAAM/ypsoljlVlyVBz0qVSHkMeMYOP59qp43KFZV4Ocd6nEse5jGoCHvgg5rNotFjc6tGigE55yP8A69SuTMcoAhXk1UjmKlclWAJ55pQ3mbjkD07VFikT7i7sAUwPY1H9oRJnGC/OBzwD/k1DLIwtiJSu4Y25AqJbobSXbP8AdwverURNk8s6qwVyAB+NQs2SEYsWdTgZ+770YPlsXUl24/CrDoBjadp6AseQMdDT2J3K8cZAJ8xsr15pV2uFyCQfVunNTwiBMgnBzzS+UWbKkewBFK47FckKJCN2QP4sfpUPJDDG5VG73NW9hLMCgClTkk1DNFCARGfmwAeeD600wIUkKHdhsNwPm6VcinUuzEq2egINUDAuADxjtnpRuwxj2DCnrjmtU7iTsa68vuT5cjmkXchZ/MwcYxiqMDOExITjsasNPGAED8nrScUWpXJ5QdgkLYOMAAUx0cRgOQdwzULTPtCO5wOmaC+WBJJG3GajRAKU5+bbz3xUckShUAJIBOe1I0u5gUyNvYkVHIWdfkDbhyRVEtjhIPKcOMpggADkVXOwRnZlmPXnGBT8lyqKAXXk560rM53EgOcYO4ZwK0SsQ9QIOyNFySOnNND7SI2cqQe3OafEbd5FCNIpzzt4rTt7C1eLc7ShyTyG/wDrU7CvYqKmFBI6dhWtax4hjbGBKMHPbrTYLO1jLHbvb1yeKsqyeSuBjHFJRQpzvsLH8knl8HHOaN+HK47d+aQFB2bPWnAREZIY5OM07GQu1gQ20cce1G4kdMZ6imNFgkBs0se6NsgfL71Goy1EfVAeO9TggDAAxUEBXJHJyPyqYGMKAQSfakMrkAgU3NHHrQQD1rnNA5CcmhCGTjnn1puBjjpT0OMYHBq4EsHULtyox9aCqswJH607OBtPc01huchTjb3rUkUop3HBwOlIM7VBHFJ+8Rs7VbPvS4A5K4+lADSdowBxmhicggZxzilJPO07h/KkJypODkjHFIDnb+J4rhjt4ZjVYBiVUYjwee9a+tJIV37gIwc+/QVjMTkMwGB19/ShGiHOqLMxyFOAMk8E08s/DyFSSOiniocMFBYFfbNDBT+8Dke2KLF3J/mkZDGD6cVJI0jL80eQPwqGMh0WQZ+U052AQqQeai2o+hC0xbk4XuAfSoPtO52woIB7UjIbgBF+UnG05/OrMcBjXbvAA6nHetNEQrsiS6fPChm64AoaaRm+dPvHOduMVKEw+FO78MUYDnAA4paFIdCsDIxebbKDwuOv40eYN2WYY7YwamjiV1U4BOMHOKZJZru5YKR1AHSjQsY5yqruBDH0pg2b2BOFUZzU62SSBFMpXnsKSe3jiYoSSoHXHWmrA1YptPEuZA3LcdcVMrAsJn/i96Y0CS8Dy9v+71p7xHOxEC56n071SSMyOSZJJvLGDj0anlBt3KjKT13Gonj+VncgjoCBQU4B3Nx0oYiyPmb5CrFevNSFioG1QfUZqskb79xwc9AKmclVChRn61m0WmE0jK5YEZI5G01HLhV/vFsZIFJlpJGYkDIxSSbpRgdE5z61pFWRDd2MB2TIAAVxkf4VIzMmWQkM/G3bTY8rhj8o+761Kkh3EHDZ4z6e9UhDrZWN0kSn+LO/HFboVlSNJGBA5471m6RasZfMDfKgzj1rTLbpPLdcE9MUMh6jjnfvx1GMCmgbevNADRsOQ39KflslAwAPepJHAMCCoyM0FD97cd3YYpoAz03Gnbthztznj6UASDaT1PPtS+WRllO49hTDwMBaehJYErjHvRuBLEc5JGHFWQAVBxUG3K5zjJqZGXaOaQyqNx6KDSL5gY5A2n9KXgdBSEsOgrmNBGAGeelOjDNEAMAE96ac4PHOKSDeYiXBzV0yZEh6kBifxpMgg7ufY0oGFHTgdRSZAOcZI61qSIOOmBSFQzYOOKcVLY3DafrQcbflbOaAEIzgY/WkwoJG5gRyOe9Lxtxgbuxwagury3tEzPcKjEE+WTgkflRuBFqYM2nS8KzJyN3AP41zUxxksVCt2wMZqXUdXnvgYpEiW3D8EDOR/OopGR4UaMjCDGKaRSBLiRGCF9wOTyKI3BQAnHHbioGYeZkuoPoBT1GSioyuw7UWKuSI5GdhyA2cEZpWlmMYOGUZ55/+vULiXcYwPqBinQhliw2EUHjHelYdydRuZAp6ZIIPNShuCJMH5jjiolkTjYM9jinjavHPqKhlIc4ZVK4w3bjiqQmMJUng96u7uVG4nNU7qJpYjtBwc9R1NERy8ixHcxv950yeeaeJQFLB0x9elZUdq4JDQnKqOR9KjSMI5RUYEjJJq+UXtGjYEoZMibJ6getJJdFEG6TBHvWV87JtD5jPHTk1LEhEQjTC5IB45p8oe0ZdifKo6bSHBH603O6ZlxnudvGaWCBI0VTufnHJ9TThH5b52gcf570210DVgANvAwAelNZSxJyTn3oBO1s4zknOKcmwZ3MKm4xg3eZlZAuPU0rqjqXB3yNwmDjn6VEyCPlgAPUU6HZwQ2WPCg9qq1yB7bBFw5Jyag3BU56du9Lcs3GX4zjpSR7S4Uy/LnkY6VbJBWUYHmfOf4c4oXbG/wAxCFjwFJOaR4lWVnR854ORir+lISZBuZVHYL/WkI2LeJI4QEzkrg808Y6Nnd7d6EwGVufenZ354Ix60bkCALuzjBpwLb/Tnp2pu1sZI+Ud+lObawVskn0FSA5eDgEBs04gk8Dd60xAGbp75707LoAQpAJoAXA37hwg60/5cFg3PYCm43FhjoenrUiAADcdpxj3pASxbmTBPyg5A71IoGOeDUdudrMpJx1qY9eAD9RQBWDbV9aN1IRSZzXMjUVycHGPzpVJ2+u7saRs7W5ApsZ2jJ6CrgKY4sgzlSuD9aMkKSNoB7d6U7SWJGQfWjAx0Ge1akIaVXJIXB+tAYZ78deKU4DYU1j6zq6rbvBbN85ba2F6fjQBLqmspYzGJYmeQqcEghe1c1ckzzCSUl5H+Qk5+UY96YGMjSy5yx6sTnNMwc4ZcN/e9PStErCBpE2yhUMZ7N1P5cVPbuDGSwOABkkfe461SAQMUExYnqakhByXLhdvYt1H0oY0yxkBg0SliRyDgYoMRRjJtG8/w9OKXzVO5c7iwzjpgdPaolCuFWNmGOD5gOaRQ7CRkORln680mTG7KpBI+akIPorbT12k4pczF5GQjYQASByKQydN7gMdqgjPympVZVZMZJHWqqyBUcqzMB0z3pyzSkDamODnIx2qWilIvpJ5bbo13ZwT7CpgweLZv3k89OlZ0TquFkJwev1zVmNn81gZd8WPlHSs2i1JlgKUXAC8gDk0jW67dysORzxSCXyhhW2jvkZpVuEOTG2c9TilqMjW3URgKB7cCpBAsAV2RSx65xSxy/KRnDH9KcC20bn3fU5xVq40V3YKMDqDn3pitkZDMxI6Ef8A16kmyqEn7/riq+Sm2Q5GOvHFUkyXIUy+YGUpgKO3Wq6urw7WwvPfPNPcElvKDMG5wOetNXyoXCzgKP8AaHerUTNyGonlkMxJHYH+tKMnLqcBsj6VEZQrckedk8joB/8AqqMww8OY8uTy+/j+dPYi5Zlw7eUv3lO45+lRFlaRgp+buM4x+lPwksTZKsM8AN7etQyxqu1xIPMBGRnsKbAniZlmU7FKjqW5/QV0dowaH5SeCc44BrlJZI9isVVhgbsHt+dWLO+aBmNvIRGccdaVgudQoAJIzn6VMp8yHnOc1Tt5VmWNlYsSOcdqsxkBQvIHcnpQkSwEmTsI+vNPSXDlSpIxxioyMkhhuXNOJAO1SVHp/wDXpXCxNvypJQD6Ub8KM5P1qNWIXaeffFL8h5OTRdgTCRd7DZz2NG9dq9j6+tM3k4APJqNpEUkPII23YBY4B9qV2MvLJ5mQTyPap1wFHNVICzD96Du+lS4A6EgUmxEOD6Um3B45pTkDIGaAMkmuU1EIPzNjHy5pUG9BkfLTWIVCeoI59qbGmQCGOwelXAUiUqdo3ISfQGkAI42bT2FGckkqQB3PWo7m4+zxNMRyoyBittyCrqt3/Z6BI2R5GBwD9K5Rgsh3OUEhyWycc0+WVppVdicqOMnJqFwCqscEuSAMc/ia0SEMHzny1VdgbO4HIP8AnFN3wl2aS52hhxx6UeS43blC85BB6D6U0ea3ywBXQDBxlcfnTEIr7AWEqhB3xT7ZVeZWDgeYpJPtzUJSZJVXyt+f7zDApwUx3KsQkZPGOefpgUMaLoB3v84dV4B/DNRblEgc4HcA0+aEBgSW2/7wIzTZATjlT6ccipRY1n3RllYKDzTXjyoU5IOT160FRLkPF9xevrRJGszMXGduMAD/ABoC4RMkMzHzPJRVzgjOTUgZZFEyfM/XGeDTFj3zNsO3IOFxTJFZmKbQGj5LDqR/kUCLKth98y7Sw9aliMWdzMfYVRMgyH3+2DkipDMka7yCyMQBt7Z780mikzQ3IwzuHNRO20rGFz7g1TE4AGzay+rjmnLMG6goR0weDSSK5i8HjDcNg5Gc96c8iAkbg2BmqRmWRfmVOoPQnFIZgR8jD06Glyj5if7S77gAuAKY91cFGCcoDyMc4qq8gSbJOT/dPSmr5aDO3lupNWrmbZYW4eVwqjY5GM4qHcQ/l3JHGOeBTWWSQsI2dmHZWxSYZkwYcOnOMjv6+tMQxi/n78Dk4qcKQmMA45xSRHzpSqxLv6ncOOOeKa+zZIQ7RSo2MAcfpQBKkhZxkFEH9/gfripARHhmnXcxIKAdPTnNVSodsbcbeevX9KuQyCSJgQSSO5zjmmBXnuJBKUSIOu3O4HrUXBjB2FTnjnPNLMWF2IfLATOd/wCBpgcFgoY/uxuPvTJNPTbt7d0RlDBh9MV0ts++3QgggjOAc4rjVXcODnPrmtfS7k2vlRtF8rcZDZFFgubx+8fmA9qVCTFx9/Hc03IGGKbl9fShNoPAyOvNZgSgOyjJ9qUK2NjABRzn1oAyMKiik3KUDlSDnHBFIBcurLhgBj0p24EFGkGCd3I70YwPvFs+tA+ZCgLHJ65oGTo7knLAn1AqXzD/AJFQx5A+7znrmpsfWkBASB1o5z0ozjqKTntXMaCsOMAD3zTYcmMcIDQdxRs+lEbiKMSNtCn1NaQFIdLKkcLTSPhF7nFcpqV8by9kZp8QqWU5yBgVc8Q30OwWsexvmOSD7ZrIZlaLYsnXAK7T7Z5rdIgheVQoOPyFLk70dVAG4YORmmMRFOYm29M5zk0xkjZt4ZXcMMgnH9asQ6T77ncpz13Nyf1qNlhLBcMqg5IHG/24p0kMM58zBRoz/DyD0old1kUlQSvKketAiJk227bS8jvwMnhaW2Rg8YkEajPbBz+dOkY4dHT5yMlemfpTLYRsFWRdoDYGOcUMpGjMI0cYwQTnA6UyZ0dOMKc8Y4p6KpLDjaPu+9MkUu0aOm3DZPNZ3NLEGGERVlGD/FnmkZtj5LLg9QetSspJwr4TsaRrcKQWZS3qTiq3JB3jMjMn3mGBjtxTQ5jjCOAGzyzd/wAaY5wjZALMcPg/dHrSRrGiGMLvQdDnoetADztjkL/d+U/jTY1dYv8AWABhyg6/kaQARn523luBkYxSuY2U7vkcc7uSD/SkA14wRlFO4HncKeqqHDFwSR3BoziMBDtB7mnEuJBhjJjHGMYouOwm0nlQWcZDBhgfhTHRdqqFKNnkHgVJKP3pYlueR3x+lRODv3uhKLjDZpoTGSRMsoAgXdj7x5p3mvtHnDK/SpJGbKlzyei460wylYwqDEncHnFFxCqUI2puDH04pI/MER8xUznO1jjI/OoTu8wFWLYHPGOaWN3JO2Df7HIxTAfHLvkYxMhjUdCDxRuWSb5XTbnlcHr+NOeBWRQgZWPO3cP60CSQW3lspIVsEDAPFADHDR+XIqoQGwcrj3qY+Z5h8tIic8Imc4/lSeY+GLREkngE+3tUkvzoytbnBBztbGM/WkMhlSR5fNKFQAA2Ox70xWiZwYlcEf3xjPtRCRFG8MSkrktgkHFNyzKQULDpnpVkEtuuS6yqyuBn/Z/OpJHcrGsOQVYHPNMiRYIlAk+Zedp706SZ3RnI6DjbzTEbelao8xeORhwejD1rb+fqcADpiuIhkkQrIcZ45PHf0rsbadriJWG0lkHCnOKzYyYEEg4YU4uF+6CM+tMDui7D846c8U5c5+U7T71ICgvu4xx604nByxGT/dpHBfO87eevrSDMYA3HZ9KLDLSBQRuBGehqcSADDdfYVVjTccg5bv7CrWAB1P5UMCsoJGAtG0Cl5Y4HFIB61ymhHKSsUhzz5ZI/WuMlmMxVppG4GNoJrtZFHkvvz/q26VxBdPLQRHdhed5rekTIj2GMMSpUd2JzSr837zG5Qe3FODxiMJIozz3z/WoyXZPlH7oHpW5A552iDHe8ee0fIP6iojIiKCQCCRzjmp9kccjqoljjwOM9agHl4IhU5HdhyaAGCYGYJnAkOB2pkkTlzmPYQP71SKGMZ3AGNScrTX2GNRswQegFAEaoWbauXdhjceNtT2haJimVOW9+tR7ZVDpOAUUZRx3qxahtgMYbOeMdB9aTGiyjggqNu8e1WGKyBULfvMEnioImlVj5pQk/3asBl2hQwZg2cVizZFWRHLKcjauQc1CyKBukIPYcVoFLdmLzOw7AA1XljIHPzL1HfFNMTRUQBNwABdiOCOnNMmKxy5kZVZQCQBkGpwY2GSGUg5ORinDlHKA46KTV3JsQLJuIeZAwJ+QL/wDXoQ/vHDgCT/nn1/WlyxZTKGk2ggY7c9adIXdcgozHoV9fegBiKG3EptK/jSEtKo2DI79qaIG/5aKCw/u08RhkXKMc0AIC0e3cML275p0chkmyE3KMjH4U0ZACsqxqvXJwaVdhOcO244BBzTEReZiRiq7pF7Z46ZpI+WZM5I9asLGiI6s04cc9KijlPy7kVo04JK9aBCRQGS4M24YTkJ24qbzSIzOMAnjbTVVJZVwjA+oWrSKUjkARgccHGKoZSBcMDgfNzn0pTgA7nIBYZxmjaqujlSCw+Zh3qRkByxRo1x8rv0NQMaiNJF877TntzTXjKRusc5ZiuMHNTblIySyOe3SopfMwWZGfI9M4oQMqwSCF0VweXPINXVRI23kthzgCqK+UN0spmJVs7cYFSJcATHDAKw4Vl5rQzFzHI7AIzfiKY7KoMYl25HQCkjZm/dqcNn7p6dakj3nBRQ8qkgoeh65oENCNGiqq5yTnJ6Cr1vO9vLG8JxtPPJGR6cUyTy2iRreJVbneMcjn/DFTtEihcIzZ65OaLAa1rrAOFuYSmeNw5rVgljlQ/MzD+H5TXICQmQrgxqOB2pVdIW80SMjngEH3pWQzs8HbtK8etO2kLgfc9652x1u4iIFy5kQjbkrn+tbVvf2dyqhZUDYzh+OfzqbDLsfJJUexqzgHvVSEmTcXMZHqDVoDjgUhEJ29yTR8vbikJ5xilIOB0FcpqQzMBBJnPEbcDvxXEKpaGEhSu7+h/wDr13cpbyZTkL+7b+VcBKVZIizlSRkcnnnpW9IiQ7zCGfeEKZ2jNNmKlEdDt2jkHpSqyKRuUqqNk7gDn/OajnaNZjMoG0gEL/8AWxWxI+8fKghVYbsb4zn/ABpjO63AC5CjqR06U9nIwwTDueVXAA/xqF1LZwcFj8yk4A/GgBszKHbfyGxj8KnV4xcE7A7bcdSMc0wrtBAPKAYGM8/WmSuxl34RGIx3waAEV3kyERlRTlVyP54zVuFdxjaTd0+76cVUXkBX2hweuOPz61aimyESTnHHy/8A16llInjYxDeqnnsanEjMAehpkCgbxkkY4yaVWAA8wnjgYFZs1RKodlJI3lvwprNhNxHOcFaWNjn90uMHPNLJtaTIbPGTxUjI3VgiRsPkHODUchZ8glto6VNIUJ37mOR0ppUtHw+DTQmisjiPPyN07en5UshKoqk7I2Py+1STBl2lTk4wcHFRBWVSxAI68nNVcVh8aSI+UYmMjk4pq4buePu5oSUv0yMUF1l5XIKjuOKAIsDzJMruPp6UzCyyDIcbDwtWoWR1JA2HbksOd34VDMCCCZMZOOhzTEIRksI4WLHq2TUSP8qARbGXHzc1NBbvucCVjjHzc1IY5JQw6benNMViPLzOgYbgOCKeFCbhjarcY60yFGEysy4696sjblhnH1FF9B2IhboE8s5+UcUjQgouwkMhyFpzSswUY+bPB9aQzhJOnzetAyFwRuOA2KqMBJG4EO0kdjU7YLNsjJ9QWAqvMIv3QLSKSeRjIoRDGIskcZiRwEPGOOPell8tZckKMD7wJpyRRK53uSCvB202Mfui7cKTgd6sgfJGhHmpcrjqQFzSqCI0Do2M5yRgEVAxCKhV9wJ5C5pzNl1fc5XGME0AXC7DaIlBQ8HFOuF8pgwRFQd2JqFCOmPl9felfC7UcBwx4z61Qhkv7z5fvFvmAWlkZmKoVUKOx6iklhZsEME4H3eopUASPy0YMM5LMDmpGSxbEkU53oe5qdZV87dHwwOA2O1UV+VCq8L61LbzAyLGpz+FNCOl0q9mVWViGHXOAP5V08UwkiV0YbSO9cdpjAs4VenvXRocxruTnHZqllFn7w6UcdxmlLqRzhT6CkDcYxXGaDXwY2G3+Bv5VwKMqRbXLBlx90Zxxmu+blH9Qp4/OuFaON5i5LgDAwvQ8CtqREiBzG0zMSZFPAL5z29aczAOqwMykj0HUU1kzId2UBI28e9JtCOoIZj81bkDfOLgfNz+FNLKoAmUnPQkdTT47dJGLyL5WOymnYDr5Y/e7c5HcUDIWMZcLghR6GkeSNiqoqNg85FTiCHAJJTPUE1CYky3ljPy9KAGpKqlgyF09T0H61bgZWUBEyuBz2FVFRlhYLtORgr6VZtCqyYLOQQDhunSk0VFlrZDhfN5yeOamlIV1VABg5xUKDkb1UgHjNThSTuKjrwayZqhu5WfBJU9CAMUoffHhV2kHrjGaGUpJudA4bBHtQCqlkPAPQ0hgQ4zvIKjpxTNy7hlSR6AU7LhMJ87LxhqcGwAxiTcR0xQBExUuSqNsxjHeonACkKG3H1NT5Vm+dFU9gKYysPvrg9/pRcViuBIRs2kEcHApJHYKFCOVXuauRgHnHHY4qOdCXKhSQRkDHSruKxDb3M4QLcKojHQhcGkaTfKVChwfX7wqRFZY03NGST02g4p5gUsSpAY9wtK4WIkDA52sPXipkbj90OSPmwOlKInKjLMR64qZEjWF+cH2ouNRIACr7i+fTINKplbflAcgelOYqEHO49yaV32MdmSD1PWmMpxrKwBZfkBzmllRsByMx5qwqnaF3YHoQKApXOcuhzgN0zQTYoxx5Z1kfaR39aRrdkyzBWVvunINaUSRnIMQUnuVqlPzIFWbOCflxVIlrQzZUliO9U3DPOcGrEYIjG4ZBzhSMinOJC20LkZ5NLkqAcZ/CrMyCOO4Mg2rGiZ6qmDUkonVWKHeB+NS7juAyVzTwh3fIw2twc96YinFLcxKSI8g9Rx/jUzGYbf9HOW7kDj9aXLK7ALs6c4qztzH80mXC5BwKQFMeaybGiHB4JwOR+NNZGPBZVQc5FWGKhPOD5boyjvUAbKk+X3oGOVWDb12Y754/nT8lCNpClmzkCjYzgMgyMZwO1LE7mTY4j4PG4cigDWsS4Lb+PT866GEv5S5GTj1Fc1auUB3NuJOMfjXRxhmjU7wOPWkwL4GCTS8DqKU0gAIya40ajXIKsduMof5VwJIgkkyyEAAnK57V30ihonX1Vh+lcIfO3KqjCkfNvA5rakRIfsneSKQu5VxhQG45GOlQSxNHdNGJDHKgzgdGB56imlWWYfMCVzznGKU7i7NIckfdOefzrYkfKpc75QfoDmotyvDuhChgwwG+tWIQZJisjZyOGY4/lTGtULO0ckbbckkNyMUAMaJ2JxgnAJKHFNmhmP7ws23uN1CrmOQ9QTgk98Ult5QyN7jHOMA0AA8xo1XbGOeCp5p0e5bgRlS2MHJIPNRKw+YBAv/TQDml3EhAollweZG6D+VAF8oS/71V6cAVa2lCo3ZJPSqMYMfIfJJ7c1ZUHepHzEjg56GsWjZMmAZCSEA3Ht3psn75htjUFeucU5fl+8xDjqad8v+8KgohCbpDgDJGab8rZTOJAe3NOlBwfK2ZPB3EjFETKUCPnePTOKYAY2wDGwOKjVjID8zMc4qSIYV/nx6AHmgHy4TjqxAIoGPjxGhBXGKRXQkt5kiN3ApF6c8fjT2YH5EQOT1PpTGRRCJ2UMXHB5ODmnlArFRkg9CaTds3Rg5II3Z7UBgeAc+goEKUUIBuPFNaQKu0L+dMDBA6tuXP60IfkyMjPQtQAxzuLZCgeopiypHlt7H2qZhnluT60qoD/Du/pTuIiBJIYkgNwBTlwhYZzt5p+35vlXp+NNztcsQAB60ANEqt84L/nVCZ4mUq6Mjrk7xzmnXcwkAGBx/dUComAjdG3YJ7HHpWkTKTI1IfMhJKLkZzTZHCEHPDHAFTAjymHO3rgUnmKdo25A5wRVECeWZAFaXGfY0iqYpFj3ZA+tBCMzPt4UcVG6Fm5fPQgnjFADpt67wMHI9cHpTw4OwIZAR1yagIMZG5yc+lOAkWT5HOCP4zTAlYhpQuWwcE/WnF9gwXAB56GiPzChST5gQcEAcH61HKq+YqxkZGMjGc0hDwA6gRvyOeaegcMHdyeccUmyNWLNx29KE2sQm4kdRigDVtE3EsN23Pet9JmCAbe1YNqPMH7qUEA84NbqbvLXntSYzXwcnNHSmq6sSuKUYP3a5EajXXduP91T/KuKltzK25ZJF2genpXbEEA89jWHJaRmbeSQcAY4rSm7Mlo5xLd2Z/MYk916cetP+xSOuY3BVPp/jXQvZ9icn1wOlM+wKdoDDb16AZrouSYJtJfP275RgAgdqDCwLCIH5+HAB5roJLDJ+ZIwPTaM0jWjnG1oxj2xSuFjANvIPlAIUD7p5xSJZeYpKTFCOowDmui+xkoD8uV+nNDWbOg37cD0FFwsc2tsEJXcQvfPNSGDERWMlkPde1b5sCjfIVOR3ApTYYT5TyOvAAzRcLHPxqYVJfdjGPmBq6sJITyGJzzjHSr39ns42hlPtViO3dPlO0YFRIuLM/7C7rukZwT9KaLcKflDmttIQUAHymlS3YE4deay1L5kY/2U7cMpbHfiozaOVLRgk/UVuG2UZLEfgKabQbcgAAjHalqF0Y0cITBkAViD260yOB3dg2RjodvFbZtghVjycY7Uv2d2YfMuPpRqO6MU2rmQJhm9xxT0tmTKDd9dtbP2cFsDbmmralGZSRn2xRqF0Y62wBbILZ6n1pRaJkODhh2xW3Hb53bV3YHtSfZychlUe1GouZGL5KO581Anp8u6kkhUAKqmRR0+XFbYs2zuDgYFPSAY+8T9cUWYc6MAQsePKxinC2cDPlsv05rdMHGPk/KkMPBwQBRqHMjCEAJ4Vh6kcVDeW4QsBk7scMM10Xl7lwcDFU5rTc7En6dKqKdyXI5Q2QGQrynilSzMg+cNwODiunawcEAkDPuKBp0gOSwFb3MzmY7NTC7HzFIJGMUq2q5B2Ef7zCun+xuoIDL+VKLIychEJHc4ouFjljZfIQT+Ax/jUYtEJCyhh6bRj6c11wsi5+YKuP7oFI9jIrZVlCnucUXCxyq2QZj5iN5fbNItmBIBsZ07V1K6dnIZ1NPFiEXnA9xii4WObFqoH3XHFM+xL95VyfpXTpZAZYOGFL9kGeNv6UXCxzX2beNskeAaEszCwMYyM9CO1dIbIOe36U8aeP7wH5UXCxmWdqI1wkHVucVtrCqqAEzx60sFt5C4BDZ+lWlj44OalsBX+XpxQCTRRXN1LEH3qjSNCeVFFFVDcGSlFwzbRmmFVMg4FFFakDgi/wB0UjRp/dFFFAwKLheBUe0FjxRRQA8qCp4HWgKuelFFMRJ5MfmD5B2/nTTGg/hFFFACqqjoBSGiioAUKCpyAaNoPaiimA/YuRwOlIVBPSiimA/ykPO0UhRfQUUUgDaNw4pQilhkCiigYCNBk7aVkUdFFFFNCIyo3ninhRkcUUUgGMoEnApERSxyo6miigBPKQnlRTnRRjAxRRVAMPBFOZQW6UUUAPEaf3RQYo8fdFFFADEUENwKcAMtwOlFFADABgDAxQoG88CiikBIVUqeBSrFHtB2CiigBVUHtS0UUAf/2Q==</FaceImg>\n" +
                "    <SignatureImg>/9j/4AAQSkZJRgABAQEBYgFiAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCABqAdkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0CiiigAooooAKKKKACiiigBKZLIsMTyyHaiKWY+gFPrlfGOsujJolipe8uwBkMBtBPA69/wCX1oA0PC2qf2rp00yQRQxJO8caRjA28HP15rarM8PaWukaPDaj7+N0h9WPX/D8KvT3MNsm+4mjiXO3c7BRn05oAlpaZFKkyB43V0PRlOQadQAtFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFITQAVHPcQ20fmTypEmcbnYKM/jXPeIfFi6bdpYWEIu72TgKGyFOcAEDv7cdqx4PB2parLcXmuXLRzOSViVwcnHGSMgAcdKANPUvHem2jFbdHumHTadoPr7/mK2dB1eLW9NW8iXYSxV0znaR2z34wfxqCy8MaTZcrZpLJjBeYbyfz4/IVo2lnbWURitYI4ULFiqLgZPU0AWKKKKACiiigApKWigBKKWop547aB5pnCRoMsx7UASUVHbXEV1Ak8EiyROMqw6GpKAFooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBKKKpatqcGk2L3Vw3AGFUdXbBIA+uKAK2va7b6NCAwaS5kB8qJRnJxwT7Z/H0qr4c0iVJJNW1RA+o3HIJ6xrjp7H+nHrUGkaRe3mpjWNaP7xc+RAQMID047Y/PNdPQAVheKvD7a/ZxpFP5U0JJTP3WzjIP5VvUlAHG+Bm1S0uLvSb6JhDbDcpbnBJ6A+h5NdlSbFDFgAGIAJxyQOn8z+dLQAtFFFABRRRQAUUlFAC0UlGaAFopKWgAopKKACjFFFAHAeO9JXTriDXLNvKcygSAZyX5O7P4fyrtdMvotS0+C8g+5Kufoe4/A5FUfFdil/4dvI3ODGhlU+hUZ/xH41D4HYHwpZLzld4OQf7x/OgDeorN1vW7PRbVprmQF8fJED8zntx6e9cfp/jjUL7WLWLZBHHLIsfl9mycZJ6g80AehUUgPFLQAUUZooAKKqanqEOmWEt1OwCoOBnG49gPrVTw/r0Gu28ksMTxmMgOG5HOeh79KANauL8YX0+o30Xh2yRg0jqZH5x2I6dh1P09q2fEHiW00e0kKSRzXXKpCrZ+b/AGsdBVLwpo0iltZ1Eh7u6/eJ/wBMwwPpxzn8KAN3SrBNM06CzjYssS43Hvzk/qat1R1PV7HSo915cKhP3U6s30H4fSrFpdQ3tslzbP5kMnKtgjP50AT0UlLQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFJRWV4j1hdF0t7nKGbIEcbH75yM/pQBY1bVLbSbNp7lwODsXPLnHT/AOvWDpWm3Ou3iaxrcSqqgfZrfHAHXJ/nz/KuduLDXvFxfU/KWMRqoiiI2hhk/dzweeSSa3fDHi2OeI22rzpDOhVEL5BftyenpQB2A6U1nVAC7BckDk45PAqOe7t7a38+aZEixkOW4PGePWvOPEfisXWvW8tn81tYuGXdwHbOSfp0/L3oA9Nqleavp9g2y6u4o3yBs3Zb8hzXHJreseK7uSz0wpaW2z5yT2HX5sZ56cVjaVpS/wDCTCz1oNhJApUnlienPoePwoA9WVldQykMpGQQcgikllSGMySuqIOrMcAfjQPLiRVG1EGFUdAOwArlfHurQxaNLYRyobiV1Vk5JC8Nn+X50AbtprWm3t01tbXaSTLnKjPOOuD0P4VfrxHR9Sn0nU0u7VQ7rkBWGcg9a6TTvGuvzTNCltHdysflXyzlfwXH60AelUE1w/8Awkvindj+wT/4DyY/PNWX0fxDrsJTVL5bCLJzDCA27pjoen1PagDoo9XsJdQawjuVa6XOUAPH49KsT3MFsAZ5o4gem9wufzrz6TwRrGm36T6XdrJg/wCsDeW6jvnn6962oPBhmuEn1XUJbog5eMkkN6c9emKAMfxj4tn+1/Y9LuDHFEfmmiblj9fStLwz4jhW2c6nqO47UClyW9c9PrWxceFdHls2t0s4ogWDb1XLD8fpWU3w900xyATziQnKNxhR6Ed6AOgg1rTLiPfHfQbQcfM+0/kaf/a+m5x/aFpz/wBN1/xrlo/h3bgjzdQlcA9kC8fnVtvAOmcFJ7pWBHO8HjPPagDcbWdMVctf24ycf6weuP8AJ/GrMNxBPu8maOTb12MDj8qxrfwho8DhvJdzjB3ueeevHftWTF4U1nS76WXRtUjWKQ9Js5I7Z4IJGaBnaZFVLvUrOzSQz3EStGpYoXG7pnGPWuTvvDPibUHDXWq27Y7AsAPoAtT2fgOARqdQu5pZD99YzhT6dRn1oAqeKPF8N3p8ljpYkdpVxJIRgAHqo9+3p9ao6NrevWmg/Z7LTi8cKtiZ0Py9/wBM55ruLHQ9M0+bzbO0SKQDaGBJ/ma0aAPG4Ly91XUGmuIJdRuCCEQqWA6ngDpjOQOlS28N/wCHfEMVxeaaXdG3eXjIOehBGRkfzFeuRQRQKVhjSNSc4RQBT8UCOP8AsGu+JXV9RJ020RcpGo+Zm9SM9vf8uTWaPButpfyrDeBIADtm8wjeCMYwOnX+dehUUAeeWDeKPD1zLbC0mvIR04aRPqp6j6USePdRhv1S4tY41iJEsQBDH2OScGvQZGWONndgqqMkk4AFeaaLokfifXr68mV47TzGkJUjO5iSF/8Ar47e9AFC71htWne51SZmj+byoQ2FQ44wPy/KpvDWq6hbLPYaZbq0t22FPOV4PI9Oufwrrf8AhANHyPnusA52+YMfyra03RdP0tFWztkQjneRlicc8mgDiPEHhG5ttMbUnuJLq8Vt0+3pt9R34/z0re0TxBY2XhqF7y9RmiGzYMlx6DH9eldLLEk8LxSqGjkUqynuDwRWOvhLRFY7bFdp/h3tj+dAHmet3z6rq8t24fy5X+XI+6vQAe+K9H0DVdFtNKt7SC/QLGn/AC2Ow5Jyc54zk9K2XsbR7dYGtYWhQ5WMxgqp9h0rFvPBejXlwZvJeEnqsLbRn1xQBsR6jZS/6u8t34J+WVT05NVn8QaTHP5TX8IbIH3uPz6ViL4A05Z2c3NyYz91dwBH4/8A1qePAOk9TNeH6yDj/wAdoA2v7d0ny2f+0bXauc/vRnj0HU1g6r42i5t9Hgmupn+VZFQ4BOOg6k/55rXtfDGj2oGyxikOMEy/Pn354zWhBZWtqc21tDDkYPlxhf5UAV9De9k0i3fUgRdsCXBABHJxwPbFX6BRQAtFJS0AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACVxOjaXP4l1F9W1pHEMT4t4iAFbB54PYY/HNdtRigArJ1fw5p2sMJLmIiUf8tEOCfr2Na1LQBy58CaWTzNdkDoC68f+O1J/whOiG5SZopHKgDYZPlPHU9/eujpaAKdjptppwcWkAi343YJ5x/8ArrI8SeFrfVVkuoFKX+PlbcQGwMYP4fSuiooA4IeCtXYxwS6kn2cfMcFjtPGeD1/+tWlH4A0wxj7TPczSd237R+XNdXRQBiaV4V0rS3WSGAvKhyskjZI4/KtaO2gilaWOCNJH+86oAW+pqWigAxRS0UAJRS0UAJS0UUAFFFFACUUtFACUUtFABSUtFABRRRQAUlFZ2uatDpGnyXEjDzMERJ/fbsPpQBz/AI/1w2lqmm2sg86c4l2nlU9D9f5fWt3w3pf9kaNDasQZfvyn1Y9fy6fhXJeEdJl1i/k1fUAjxFySrc7364x2AzmvQaACloooAKKKKACiiigBKWiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAEpaKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAErL1fQ7XWTF9reXbHnCIQBz17VqUUAQWVlb2FstvaxCKJeij/E1PRRQAtFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABSUtFACUtFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9k=</SignatureImg>\n" +
                "    <DocumentClass>DriversLicense</DocumentClass>\n" +
                "    <DocumentStatus>Attention</DocumentStatus>\n" +
                "    <DocumentExpired>Document Expired</DocumentExpired>\n" +
                "    <DocumentExpiredDescription>Checked if the document is expired.</DocumentExpiredDescription>\n" +
                "    <DocumentExpiredStatus>Attention</DocumentExpiredStatus>\n" +
                "    <BirthDateVaild>Birth Date Valid</BirthDateVaild>\n" +
                "    <BirthDateDescription>Verified that the birth date is valid.</BirthDateDescription>\n" +
                "    <BirthDateStatus>Passed</BirthDateStatus>\n" +
                "    <ExpirationDateVaild>Expiration Date Valid</ExpirationDateVaild>\n" +
                "    <ExpirationDateDescription>Verified that the expiration date is valid.</ExpirationDateDescription>\n" +
                "    <ExpirationDateStatus>Passed</ExpirationDateStatus>\n" +
                "    <IssueDateVaild>Issue Date Valid</IssueDateVaild>\n" +
                "    <IssueDateDescription>Verified that the issue date is valid.</IssueDateDescription>\n" +
                "    <IssueDateStatus>Passed</IssueDateStatus>\n" +
                "</VerificationAppResponse>";
    }
}
