package zalacompany.verificationprocess.fragments;

import android.support.v4.app.Fragment;

import zalacompany.verificationprocess.MainActivity;

/**
 * Created by optimtec006 on 26/03/2017.
 */

public class BaseFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() instanceof MainActivity)
            ((MainActivity)getActivity()).setCurrentFragment(this);
    }

    /**
     * Just a simple way to call DashboardActivity class.
     * @return
     */
    protected MainActivity getMainActivity(){
        return (MainActivity) getActivity();
    }

}
